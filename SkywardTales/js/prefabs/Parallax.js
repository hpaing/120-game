/*
120 - Final Project 
*/

"use strict";

function Parallax(game, image, x, y, speed, flag) {
	var all = ['bcloud-1', 'bcloud-2', 'bcloud-3', 'bcloud-4', 'wind-1', 'wind-2', 'wind-3', 'wind-4', 'wind-5', 'wind-6', 'wind-7', 'breeze'];
	if (flag == 0) {
		all = ['bcloud-1', 'bcloud-2', 'bcloud-3', 'bcloud-4'];
	}
	//creates a parallax offscreen on the right
	Phaser.Sprite.call(this, game, x, y, image, Phaser.ArrayUtils.getRandomItem(all));

	var magnify = Math.random() * 0.5;

	//Flag key: 0 = foreground, everything else (1) = background
	if (flag == 0) {
		magnify += 0.5;
	}

	this.anchor.set(0.5);								//changes anchor point to the middle for proper rotation
	this.alpha = magnify;								//gives random alpha value
	if (flag == 0) {
		this.alpha = 0.9;
	}
	game.physics.arcade.enable(this);
	this.body.velocity.x = speed * magnify * 2;			//assign travel speed of parallax
	this.body.velocity.y = 0;
	var scale = magnify;
	this.scale.setTo(scale, scale);

	//color inversion
}

//explicit cloudconstructor declaration
Parallax.prototype = Object.create(Phaser.Sprite.prototype);
Parallax.prototype.constructor = Parallax;

Parallax.prototype.update = function() {
	//destroys backgrounds that have passed to the left fully
	if (this.position.x < -250 || this.position.y < -250) {
		this.destroy();
		//console.log('Eliminated parallax');
	}
	//destroys parallax generated with miniscule alpha value
	if (this.alpha < 0.15) {
		this.destroy();
		//console.log('Poor alpha value');
	}
}