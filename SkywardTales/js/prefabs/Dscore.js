/*
120 - Final Project 
*/

"use strict";

function Dscore(game, image) {
	//creates a parallax offscreen on the right
	Phaser.Sprite.call(this, game, game.width + 300, (Math.random() * (game.height - 200)) + 150, image, 'devhs');

	this.anchor.set(0.5);								//changes anchor point to the middle for proper rotation
	game.physics.arcade.enable(this);
}

//explicit cloudconstructor declaration
Dscore.prototype = Object.create(Phaser.Sprite.prototype);
Dscore.prototype.constructor = Dscore;

Dscore.prototype.update = function() {
	//destroys asset that have passed out of bounds fully
	if (this.position.x < -250 || this.position.y < -250) {
		this.destroy();
		//console.log('Eliminated dscore');
	}
}