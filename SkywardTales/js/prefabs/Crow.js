/*
120 - Final Project 
*/

"use strict";

function Crow(game, image, x, y, speed, type) {
	if (type == 0) {
		Phaser.Sprite.call(this, game, x, y, image, 'dartB');
		this.animations.add('dart', ['dartB'], 1, true);
		this.animations.play('dart');
	}
	else {
		Phaser.Sprite.call(this, game, x, y, image, 'sweepB');
		this.animations.add('sweep', ['sweepB'], 1, true);
		this.animations.play('sweep');
	}
	game.physics.arcade.enable(this);
	this.anchor.set(0.5);
	this.scale.x = 0.50;
	this.scale.y = 0.05;
	this.body.velocity.x = 300;
	this.body.gravity.x = 500;
	this.alpha = 0.60;

	// this.body.velocity.x = -400;
	// this.body.gravity.x = -800;
	// game.time.events.repeat(5, 200, makeStreak, this, streakGroup, type);
}

//explicit crow declaration
Crow.prototype = Object.create(Phaser.Sprite.prototype);
Crow.prototype.constructor = Crow;

Crow.prototype.update = function() {
	if (this.scale.y < 0.50) {
		this.scale.y += 0.01;
	}
	if (this.position.x > game.width + 200) {
	// 	this.body.velocity.x = -400;
	// 	this.body.gravity.x = -800;
	// 	this.scale.setTo(1, 1);
	// 	this.alpha = 1;
	// }
	// if (this.position.x < -120) {
		this.destroy();
	}
}