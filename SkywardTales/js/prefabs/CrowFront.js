/*
120 - Final Project 
*/

"use strict";

function CrowFront(game, image, x, y, speed, streakGroup, type) {
	if (type == 0) {
		Phaser.Sprite.call(this, game, x, y, image, 'dart1');
		this.animations.add('dart',  Phaser.Animation.generateFrameNames('dart', 1, 4, '', 1), 20, true);
		this.animations.play('dart');
	}
	else {
		Phaser.Sprite.call(this, game, x, y, image, 'sweep1');
		this.animations.add('sweep',  Phaser.Animation.generateFrameNames('sweep', 1, 4, '', 1), 20, true);
		this.animations.play('sweep');
	}
	game.physics.arcade.enable(this);
	this.anchor.set(0.5);
	this.body.velocity.x = -300;
	this.body.gravity.x = -800;
	this.body.immovable = true;
	//changes hitboxes
	if (type == 0) {
		this.body.setSize(100, 36, 3, 14);
	}
	else {
		this.body.setSize(60, 200, 30, 10);
	}
	game.time.events.repeat(20, 200, makeStreak, this, streakGroup, type);
}

//explicit crow declaration
CrowFront.prototype = Object.create(Phaser.Sprite.prototype);
CrowFront.prototype.constructor = CrowFront;

CrowFront.prototype.update = function() {
	if (this.position.x < -120) {
		this.destroy();
	}
}

function makeStreak(streakGroup, type) {
	if (type == 0) {
		if (this.position.x > -100) {
			//main streak
			var trail = game.add.sprite(this.position.x + 80, this.position.y + 10, 'crow', 'streamL');
			trail.anchor.set(0.5);
			trail.alpha = 0.2;
			var streak = game.add.tween(trail).to( {alpha: 0}, 300, "Linear", true);
			streak.onComplete.add(destroyStreak, this, streak);
			streakGroup.add(trail);
			//secondary streak
			trail = game.add.sprite(this.position.x + 70, this.position.y - 25, 'crow', 'streamS');
			trail.anchor.set(0.5);
			trail.alpha = 0.2;
			streak = game.add.tween(trail).to( {alpha: 0}, 600, "Linear", true);
			streak.onComplete.add(destroyStreak, this, streak);
			streakGroup.add(trail);
		}
	}
	else {
		if (this.position.x > -100) {
			//main streak
			var trail = game.add.sprite(this.position.x + 80, this.position.y, 'crow', 'streamL');
			trail.anchor.set(0.5);
			trail.alpha = 0.2;
			var streak = game.add.tween(trail).to( {alpha: 0}, 300, "Linear", true);
			streak.onComplete.add(destroyStreak, this, streak);
			streakGroup.add(trail);
			//secondary streaks
			trail = game.add.sprite(this.position.x + 70, this.position.y - 90, 'crow', 'streamS');
			trail.anchor.set(0.5);
			trail.alpha = 0.2;
			streak = game.add.tween(trail).to( {alpha: 0}, 600, "Linear", true);
			streak.onComplete.add(destroyStreak, this, streak);
			streakGroup.add(trail);
			trail = game.add.sprite(this.position.x + 70, this.position.y + 90, 'crow', 'streamS');
			trail.anchor.set(0.5);
			trail.alpha = 0.2;
			streak = game.add.tween(trail).to( {alpha: 0}, 600, "Linear", true);
			streak.onComplete.add(destroyStreak, this, streak);
			streakGroup.add(trail);
		}
	}
}

function destroyStreak(streak) {
	streak.destroy();
}