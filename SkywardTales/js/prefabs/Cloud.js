/*
120 - Final Project 
*/

"use strict";

function Cloud(game, image, speed, flag, xAxis, yAxis, asset) {

//	var cloudList = ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9'];

	//creates a cloud offscreen on the right
	if (flag == 1) {
		Phaser.Sprite.call(this, game, 650, 700, image, 'mc');
	}
	else if (flag == 2) {
		Phaser.Sprite.call(this, game, 950, 700, image, 'mc');
	}
	else if (flag == 3) {
		Phaser.Sprite.call(this, game, 950, 550, image, 'mc');
    }
	else if (flag == 4) {
		Phaser.Sprite.call(this, game, 1250, 555, image, 'mc');
	}
	else {//*****************************
		Phaser.Sprite.call(this, game, xAxis, yAxis, image, asset);
	}
	this.scale.setTo(0.4,0.4);
	
	if(flag == 1 || flag == 2){
	   this.anchor.set(0.5);	
	}
	this.alpha = 0.95;
	//this.anchor.set(0.5);								//changes anchor point to the middle for proper rotation

	var flip = Phaser.ArrayUtils.getRandomItem([-1, 1]);//randomly flips some cloud sprites

	this.scale.x *= flip;

	game.physics.arcade.enable(this);
	this.body.friction.x = 0;
	this.body.friction.y = 0;
	this.body.velocity.x = speed;						//assign travel speed of cloud
	this.body.immovable = true;							//makes object immovable
}

//explicit cloudconstructor declaration
Cloud.prototype = Object.create(Phaser.Sprite.prototype);
Cloud.prototype.constructor = Cloud;

Cloud.prototype.update = function() {
	//destroys clouds that have passed to the left fully
	if (this.position.x < -1850 || this.position.y < -100) {
		this.destroy();
		//console.log('Eliminated cloud');
	}
}