/*
120 - Final Project 
*/

"use strict";

function Puff(game, image, x, y) {
	//creates a puff at player position
	Phaser.Sprite.call(this, game, x + (20 - Math.random() * 40), y + (20 - Math.random() * 40), image, 'dust');
	var puffSize = Math.random();
	this.scale.setTo(puffSize, puffSize);
	this.anchor.set(0.5);								//changes anchor point to the middle for proper rotation
	this.angle = Math.random() * 360;

	var flip = Phaser.ArrayUtils.getRandomItem([-1, 1]);//randomly flips some puff sprites

	this.scale.x *= flip;

	game.physics.arcade.enable(this);
	this.body.velocity.x = 100 - (Math.random() * 200);	//assign travel speed of puff
	this.body.velocity.y = 100 - (Math.random() * 200);
	this.body.immovable = true;							//makes object immovable
}

//explicit puff constructor declaration
Puff.prototype = Object.create(Phaser.Sprite.prototype);
Puff.prototype.constructor = Puff;

Puff.prototype.update = function() {
	//destroys puffs that have become invisible
	if (this.alpha <= 0) {
		this.destroy();
		//console.log('Eliminated puffs');
	}
	else {
		this.alpha -= 0.04;
	}
	if (this.scale.x < 0) {
		this.angle -= 25;
	}
	else {
		this.angle += 25;
	}
}