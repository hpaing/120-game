 /*
120 - Final Project 
*/

var game = new Phaser.Game(800, 600, Phaser.AUTO, 'Game');

var templeIn;
var templeOut;
var tplcloudFront;
var tplcloudBack;
var wall;
var floor;
var invisDoor;
var baseFloor;
var topFloor;
var wallL;
var wallR;
var TEMPLE;
var backFrom3;
var lantern;
var light;

game.state.add('Start', Start);
game.state.add('Play', Play);
game.state.add('Play2', Play2);
game.state.add('Play3', Play3);
game.state.add('End', End);
game.state.start('Start');