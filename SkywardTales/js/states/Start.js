 /*
120 - Final Project 
*/

"use strict";

var Start = function(game){
	this.player;
	this.gamename; 
	this.instructions;
	this.parallax;
	this.foreground;
	this.credits;
};
Start.prototype = {
	init: function() {
		this.parallax;
		this.foreground;
		this.bgm;
		this.gamename;
		this.instructions;
		this.spc;
		this.space;
		this.msc;
		this.mouse;
		this.credits;
		this.ready = false;
		this.processing = false;
		this.sccloud;
		this.display;
		this.score = 0;
		this.next3 = false; //***********************************************
	},
	preload: function() {
		//for testing FPS
		game.time.advancedTiming = true;
		//preloads images to be used by the game
		game.load.atlas('pc', 'assets/img/player.png', 'assets/img/player.json');
		game.load.atlas('parallax', 'assets/img/parallax.png', 'assets/img/parallax.json');
		game.load.atlas('clouds', 'assets/img/cloudforms.png', 'assets/img/cloudforms.json');
		game.load.atlas('misc', 'assets/img/misc.png', 'assets/img/misc.json');
		game.load.atlas('tornado', 'assets/img/tornado.png', 'assets/img/tornado.json');
		game.load.atlas('crow', 'assets/img/crow.png', 'assets/img/crow.json');
		game.load.atlas('tCloud', 'assets/img/tcloud.png', 'assets/img/tcloud.json');
		game.load.atlas('temple', 'assets/img/temple.png', 'assets/img/temple.json');
		game.load.atlas('sunery', 'assets/img/sunery.png', 'assets/img/sunery.json');
		game.load.image('templeIn', 'assets/img/templeIn.png');
		game.load.image('templeOut', 'assets/img/templeOut.png');
		game.load.image('tplcloudFront', 'assets/img/tplcloudFront.png');
		game.load.image('tplcloudBack', 'assets/img/tplcloudBack.png');
		game.load.image('bar', 'assets/img/bar2.png');
		//mural list; will expand with new murals
		game.load.image('mural1', 'assets/img/murals/mural1.png');
		game.load.image('mural2', 'assets/img/murals/mural2.png');
		game.load.image('mural3', 'assets/img/murals/mural3.png');
		game.load.image('mural4', 'assets/img/murals/mural4.png');
		//sound effect from https://www.freesfx.co.uk/sfx/poof
		game.load.audio('poof', 'assets/audio/poof.mp3');
		//sound effect from https://opengameart.org/content/jump-landing-sound
		game.load.audio('land', 'assets/audio/land.mp3');
		//bgm legally purchased on Pond5.com; DO NOT STEAL
		game.load.audio('bgm', 'assets/audio/fujidawn.wav');
	},
	create: function() {
		// *New world bounds
		game.world.setBounds(0, 0, 800, 2750);                   //***********************************

		//creates ARCADE physics system
		game.physics.startSystem(Phaser.Physics.ARCADE);
		//prevents tunnelling
		game.physics.arcade.OVERLAP_BIAS = 10;

		//creates necessary groups
		this.parallax = game.add.group();
		this.foreground = game.add.group();

		//creates several assets prior to the start of the game so the game doesn't start blank
		for (var x = 0; x < 15; x++) {
			var newParallax = new Parallax(game, 'parallax', game.rnd.integerInRange(0, game.width), game.rnd.integerInRange(0, 600), -200, 3);
			game.add.existing(newParallax);
			this.parallax.add(newParallax);
			console.log('Created starting parallax');
		}

		//prepares score cloud and display offscreen
		this.sccloud = game.add.sprite(845, 355, 'misc', 'sc1');
		this.sccloud.alpha = 0.9;
		this.sccloud.scale.setTo(0.5,0.5);
				    	//this.sccloud.anchor.set(0.5);
		this.sccloud.animations.add('fly', Phaser.Animation.generateFrameNames('sc', 1, 5, '', 1), 20, true);
		this.sccloud.animations.play('fly');
		//creates score text
		this.display = game.add.text(950, 385, 'Distance: ' + this.score + 'm', {font: 'Verdana', fontSize: '20px', fill: '#000000'});
		
		//enables mouse input
		game.input.mouse.capture = true;

		//sets background color
		console.log('Entered state [Start]');
		game.stage.backgroundColor = "#ffffff";

		//creates title assets out of screen
		this.gamename = game.add.sprite(game.width + 500, 600, 'misc', 'title');
		this.spc = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'spc1');
	this.spc.animations.add('spacecloud', Phaser.Animation.generateFrameNames('spc', 1, 2, '', 1), 20, true);
	this.spc.animations.play('spacecloud');
		this.spc.anchor.set(0.5);
		this.space = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'spacebar1');
		this.space.animations.add('press', Phaser.Animation.generateFrameNames('spacebar', 1, 2, '', 1), 3, true);
		this.space.animations.play('press');
		this.space.anchor.set(0.5);
		this.msc = game.add.sprite(game.width + 700, 600 - 200, 'misc', 'msc1');
		this.msc.animations.add('mousecloud', Phaser.Animation.generateFrameNames('msc', 1, 2, '', 1), 20, true);
		this.msc.animations.play('mousecloud');
		this.msc.anchor.set(0.5);
		this.mouse = game.add.sprite(game.width + 700, 600 - 200, 'misc', 'mouse1');
		this.mouse.animations.add('click', Phaser.Animation.generateFrameNames('mouse', 1, 2, '', 1), 3, true);
		this.mouse.animations.play('click');
		this.mouse.anchor.set(0.5);
		this.instructions = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'inst');
		//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
		this.credits = game.add.sprite(game.width + 700, game.height + 200, 'misc', 'name');
		//tweens title assets in
		var delay = game.add.tween(this.gamename).to( { x: 200, y: 90 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.spc).to( { x: 350, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.space).to ( { x: 350, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.msc).to( { x: 550, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
  	game.add.tween(this.mouse).to ( { x: 550, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		var delay = game.add.tween(this.instructions).to ( { x: 250, y: 300 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
		game.add.tween(this.credits).to ( { x: 570, y: 450 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		delay.onComplete.add(start, this);

		//creates player
		console.log('test');
		this.player = game.add.sprite(50, -50, 'pc', 'run1');
		this.player.anchor.set(0.5);
		this.player.animations.add('run', Phaser.Animation.generateFrameNames('run', 1, 4, '', 1), 20, true);
		this.player.animations.add('jump', Phaser.Animation.generateFrameNames('jump', 1, 2, '', 1), 20, true);
		this.player.animations.add('drop', Phaser.Animation.generateFrameNames('drop', 1, 2, '', 1), 20, true);
		this.player.animations.add('flail', Phaser.Animation.generateFrameNames('flail', 1, 2, '', 1), 20, true);
		this.player.animations.add('magic', Phaser.Animation.generateFrameNames('magic', 1, 2, '', 1), 20, true);
		this.player.animations.add('flex', Phaser.Animation.generateFrameNames('flex', 1, 2, '', 1), 20, true);
		this.player.animations.add('chill', Phaser.Animation.generateFrameNames('chill', 1, 2, '', 1), 20, true);
		this.player.animations.add('spin', Phaser.Animation.generateFrameNames('spin', 1, 2, '', 1), 20, true);
		this.player.animations.add('ride', Phaser.Animation.generateFrameNames('ride', 1, 4, '', 1), 20, true);
		this.player.animations.add('roll', Phaser.Animation.generateFrameNames('roll', 1, 3, '', 1), 20, true);
		this.player.animations.add('dazed', Phaser.Animation.generateFrameNames('dazed', 1, 4, '', 1), 20, true);
		this.player.animations.add('leap', Phaser.Animation.generateFrameNames('leap', 1, 2, '', 1), 20, true);
		this.player.animations.add('fall', Phaser.Animation.generateFrameNames('fall', 1, 2, '', 1), 20, true);
		this.player.animations.add('crouch', Phaser.Animation.generateFrameNames('crouch', 1, 3, '', 1), 20, true);
		this.player.animations.add('launch', Phaser.Animation.generateFrameNames('launch', 1, 3, '', 1), 20, false);
		this.player.animations.add('backflip', Phaser.Animation.generateFrameNames('backflip', 1, 3, '', 1), 15, true);
		this.player.animations.add('swirl', Phaser.Animation.generateFrameNames('swirl', 1, 4, '', 1), 20, false);
		this.player.animations.add('idle', Phaser.Animation.generateFrameNames('idle', 1, 4, '', 1), 10, true);
		this.player.animations.add('dive', Phaser.Animation.generateFrameNames('dive', 1, 2, '', 1), 20, true);
		//enable physics for player and its properties
		game.physics.arcade.enable(this.player);
		this.player.animations.play('drop');
		//this.player.body.setSize(70, 83, 0, 0);
		this.player.body.bounce.y = 0;				//modifies player's bounce value upon collision
		this.player.body.gravity.y = 0;				//modifies player's gravity, pulling it down over time
		this.player.body.velocity.y = 40;
		//this.player.loadTexture('ninja', 'land');

		//creates parallax time interval
		game.time.events.loop(Phaser.Timer.SECOND * 0.2, spawnParallaxUp, this, this.parallax, 1);
		game.world.sendToBack(this.parallax);
		game.time.events.loop(Phaser.Timer.SECOND * 1, spawnParallaxUp, this, this.foreground, 0);
		game.world.bringToTop(this.foreground);

		//creates the bgm
		this.bgm = game.add.audio('bgm');

		game.sound.setDecodedCallback([this.bgm], playmusic, this);

		// add tilesprites (x, y, width, height, key)
		// var newtCloud = game.add.tileSprite(0, 620, 2106, 400, 'tCloud');
		// newtCloud.alpha = 0.95;
		// game.physics.arcade.enable(newtCloud);
		// this.tcloud.add(newtCloud);
		
		// newtCloud = game.add.tileSprite(-300, 1250, 2106, 400, 'tCloud');
		// newtCloud.alpha = 0.95;
		// game.physics.arcade.enable(newtCloud);
		// this.tcloud.add(newtCloud);
		// game.world.bringToTop(this.tcloud);

		// Spawn laggy stuff here (phase 3) floors could be jumped through
		floor = game.add.group();
		wall = game.add.group();
		//TEMPLE = game.add.group();

		templeIn = game.add.sprite(2207, -3760, 'templeIn');
		templeIn.anchor.x = 0.5;
		templeIn.x = 4000;
		game.physics.arcade.enable(templeIn);
		templeOut = game.add.sprite(2207, -3760, 'templeOut');
		templeOut.anchor.x = 0.5;
		templeOut.x = 4000;
		game.physics.arcade.enable(templeOut);
		templeOut.body.setSize(100, 4302, 754, 0);

	},
	update: function() {
		if (this.player.y >= 200) {
			this.player.body.velocity.y = 0;
		}
		//proceeds to play state only after introductions are done and SPACEBAR is pressed
		if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.ready && !this.processing) {
			//prevents this if statement from occuring more than once, which causes strange behavior
			this.processing = true;
			//tweens title assets offscreen to be destroyed later
			var delay = game.add.tween(this.gamename).to( { x: -400, y: -550 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			game.add.tween(this.instructions).to( { x: -450, y: 100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.spc).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.space).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
			game.add.tween(this.credits).to ( { x: -400, y: 300 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			delay.onComplete.add(next, this);
		}
		/*
		if (game.input.keyboard.isDown(Phaser.Keyboard.X) && this.ready && !this.processing) {
			//prevents this if statement from occuring more than once, which causes strange behavior
			this.processing = true;
			//tweens title assets offscreen to be destroyed later
			var delay = game.add.tween(this.gamename).to( { x: -400, y: -550 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			game.add.tween(this.instructions).to( { x: -450, y: 100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.spc).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.space).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
			game.add.tween(this.credits).to ( { x: -400, y: 300 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			delay.onComplete.add(tutorial, this);
		}
		*/
		this.parallax.setAll('body.velocity.y', (this.player.body.velocity.y * -2) - 120);
		this.foreground.setAll('body.velocity.y', (this.player.body.velocity.y * -2) - 120);
	
	}
}

//creates parallax assets on an interval specified by a timer from the right of the screen
function spawnParallax(flag) {
	if (this.parallax.countLiving(this) < 40) {
		var speed = -200;
		if (flag == 0) {
			speed = -400;
		}
		if(flag == 3){
			var p = new Parallax(game, 'parallax', game.rnd.integerInRange(game.width + 300, game.width + 500), game.rnd.integerInRange(795, 2750), -200, 1);
		}else{
			var p = new Parallax(game, 'parallax', game.rnd.integerInRange(game.width + 300, game.width + 500), game.rnd.integerInRange(0, 600), speed, flag);
		}
		game.add.existing(p);
		if (flag == 1 || flag == 3) {
			this.parallax.add(p);
		}
		else {
			this.foreground.add(p);
		}
		//console.log('Created parallax');
	}
	else {
		console.log('Max limit reached, skip generation cycle to save FPS');
	}
}

//creates parallax assets on an interval specified by a timer from below the screen
function spawnParallaxUp(layer, flag) {
	var speed = -200;
	if (flag == 0) {
		speed = -400;
	}
	var p = new Parallax(game, 'parallax', game.rnd.integerInRange(100, game.width + 500), 1000, speed, flag);
	game.add.existing(p);
	layer.add(p);
	//console.log('Created parallax');
}

//plays the background music once decrypted
function playmusic() {
	this.bgm.loopFull(0.5);
}

//cleans up and moves to the Play state
function next() {
	//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
	this.credits.destroy();
	this.instructions.destroy();
	this.gamename.destroy();
	this.msc.destroy();
	this.mouse.destroy();
	this.spc.destroy();
	// this.space.destroy();
	//game.add.tween(this.tcloud.position).to({ x: game.world.width/2, y: game.world.height/2}, 600, Phaser.Easing.Quadratic.InOut, true);
	// this.tcloud.setAll('body.velocity.y', -730);
	// this.tcloud.setAll('body.velocity.x', -400);
	game.time.events.add(Phaser.Timer.SECOND * 0.6, enterP1, this);
}

//sets 'ready' boolean to true once tweens have completed
function start() {
	this.ready = true;
}

// *Start the game and fix tcloud
function enterP1(){
	backFrom3 = false;
	game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, 0, false, false, false);
}

function enterP1t(){
	backFrom3 = false;
	game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, 0, false, false, true);
}

function tutorial() {
	//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
	this.credits.destroy();
	this.instructions.destroy();
	this.gamename.destroy();
	this.msc.destroy();
	this.mouse.destroy();
	this.spc.destroy();
	// this.space.destroy();
	//game.add.tween(this.tcloud.position).to({ x: game.world.width/2, y: game.world.height/2}, 600, Phaser.Easing.Quadratic.InOut, true);
	// this.tcloud.setAll('body.velocity.y', -730);
	// this.tcloud.setAll('body.velocity.x', -400);
	game.time.events.add(Phaser.Timer.SECOND * 0.6, enterP1t, this);
}











