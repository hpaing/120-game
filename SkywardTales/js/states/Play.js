 /*
120 - Final Project 
*/

"use strict";

var Play = function(game){
	//important variables for usage in Play state
	this.player;
	this.cloud;
	this.jump;
	this.jumping;
	this.JUMP_SPEED;
	this.teleporting;
	this.debug;
	this.grounded;
	this.foreground; 
	this.started;
	this.display;
	this.sccloud;
	this.smoke;
    this.ride;
	this.FIRSTPHASE;
	this.SHIFTINGPHASE;
	this.SECONDPHASE;
	this.ridingCloud;
	this.RCSpawned;
	this.spawnCloud1;
	this.spawnCloud2;
	this.oneTwo;
	this.twoOne;
	this.backTime;
    this.disable;
	this.telecool;
	this.counter;
	this.tcloudFront;
	this.tcloudBack;
	this.next3;
	this.mural;
	this.flickering;
	this.e
	this.tutorial;
	this.ready;
	this.egg;
	this.instructions;
    this.spc;
	this.space;
	this.msc;
	this.mouse;
	this.apple;
};

Play.prototype = {
	init: function(player, parallax, foreground, score, display, sccloud, next3, score2, tut, tut2, tutorial) {
		this.player = player;
		this.jump = 0;
		this.jumping = true;
		this.JUMP_SPEED = -600;
		this.teleporting = true;
		this.grounded = false;
		this.debug = false;
		this.parallax = parallax;
		this.foreground = foreground;
		this.started = false;
		this.score = score;							//<---- NOTE: this will be the value observered to move between states.
		this.display = display;						//      [score] is equivalently time, so when it reaches let's say 60 secs,
		this.sccloud = sccloud;						//      an if() statement in the update loop will move on to the next stage.
		this.dropState = false;
		this.tpState;
		this.smoke;
		this.destination;
		this.difficulty = 0.7;
        this.ride;
		this.score2 = score2;
        this.MOVETONEXT = false;					//Boolean switched true when coniditions are met to move to next phase
		this.FIRSTPHASE = true;                     // *Variable indicating the state the game is in.
		this.SHIFTINGPHASE = false;
		this.SECONDPHASE = false;
		this.RCSpawned = false;
		this.shiftPhase = 0;
		this.oneTwo = false;
		this.twoOne = false;
		this.backTime = 3;
		this.debris = null;
		this.disable = false;
		this.telecool = true;
        this.counter = 0;
		this.counter2 = 3;
		this.tut = tut;
		this.tut2 = tut2;
		this.flickering = true;
		this.e = true;
		this.tutorial = tutorial;
		this.ready = false;
		this.egg = false;
		this.apple = false;
		
		//preparation variables for tornado spawn
		this.tornado;
		this.windupDone = false;
		this.tcloudFront = game.add.group();
		this.tcloudBack = game.add.group();
		this.rgb = 0xff;
		this.next3 = next3;
		this.MOVETO3 = false;
	},
	
	create: function() {
		
		this.spc = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'spc1');
    	this.spc.animations.add('spacecloud', Phaser.Animation.generateFrameNames('spc', 1, 2, '', 1), 20, true);
	    this.spc.animations.play('spacecloud');
		this.spc.anchor.set(0.5);
		this.space = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'spacebar1');
		this.space.animations.add('press', Phaser.Animation.generateFrameNames('spacebar', 1, 2, '', 1), 3, true);
		this.space.animations.play('press');
		this.space.anchor.set(0.5);
		this.msc = game.add.sprite(game.width + 700, 600 - 200, 'misc', 'msc1');
		this.msc.animations.add('mousecloud', Phaser.Animation.generateFrameNames('msc', 1, 2, '', 1), 20, true);
		this.msc.animations.play('mousecloud');
		this.msc.anchor.set(0.5);
		this.mouse = game.add.sprite(game.width + 700, 600 - 200, 'misc', 'mouse1');
		this.mouse.animations.add('click', Phaser.Animation.generateFrameNames('mouse', 1, 2, '', 1), 3, true);
		this.mouse.animations.play('click');
		this.mouse.anchor.set(0.5);
		this.instructions = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'inst');

		if(this.tutorial) {
			game.add.tween(this.spc).to( { x: 290, y: 100 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.space).to ( { x: 290, y: 100 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: 510, y: 100 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to ( { x: 510, y: 100 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			var delay = game.add.tween(this.instructions).to ( { x: 200, y: 180 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			
			delay.onComplete.add(starts, this);
		}
		
		
		//repositions temples
		templeIn.x = 4000;
		templeOut.x = 4000;
		templeIn.body.velocity.x = 0;
		templeIn.body.acceleration.x = 0;
		templeIn.body.velocity.y = 0;
		templeIn.body.acceleration.y = 0;
		templeIn.y = -3760;
		templeOut.y = -3760;
		templeOut.body.velocity.x = 0;
		templeOut.body.acceleration.x = 0;
		templeOut.body.velocity.y = 0;
		templeOut.body.acceleration.y = 0;
		//creates array of tpState possibilities
		this.tpState = ['flex', 'chill', 'flail', 'magic', 'spin'];
        
		//creates tornado animation frames
		this.tornado = game.add.sprite(-1000, -1000, 'tornado', 'tornado01');
		this.tornado.animations.add('windup', Phaser.Animation.generateFrameNames('windup', 1, 12, '', 2), 10, false);
		this.tornado.animations.add('funnel', Phaser.Animation.generateFrameNames('tornado', 1, 12, '', 2), 20, true);
		this.tornado.animations.add('diminish', Phaser.Animation.generateFrameNames('windup', 1, 12, '', 2).reverse(), 10, false);
		this.tornado.anchor.setTo(0.5);
		this.tornado.alpha = 0;
		this.tornado.scale.setTo(1.3,1.3);
		this.tornado.x = game.width/2;
		this.tornado.y = game.height/2 + 20;
        
		game.physics.arcade.enable(this.sccloud);
		game.physics.arcade.enable(this.tornado);
        
		
		game.add.tween(this.display).to( { x: 485, y: 185 }, 1000, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.sccloud).to( { x: 395, y: 155 }, 1000, Phaser.Easing.Quadratic.InOut, true);
		game.time.events.loop(Phaser.Timer.SECOND , timeScore, this);
		game.time.events.loop(Phaser.Timer.SECOND * 0.1 , timeScoreOof, this);
		//creates clouds on a set interval
		this.player.body.gravity.y = 1800;
		this.cloud  = game.add.group();

		game.physics.arcade.enable(this.cloud);
		
		if(!this.tutorial) { 
			this.spawnCloud1 = game.time.create(false);
			this.spawnCloud1.loop(Phaser.Timer.SECOND * 0.6, spawnCloud, this, 1);
			this.spawnCloud1.start();
			this.spawnCloud2 = game.time.create(false);
			this.spawnCloud2.loop(Phaser.Timer.SECOND * 0.38, spawnCloud, this, 2);
			this.spawnCloud2.start();
			this.spawnCloud4 = game.time.create(false);
			this.spawnCloud4.loop(Phaser.Timer.SECOND * 0.39, spawnCloud, this, 4);
			this.spawnCloud4.start();
		} else {

			this.spawnCloud4 = game.time.create(false);
			this.spawnCloud4.loop(Phaser.Timer.SECOND * 0.19, spawnCloud, this, 4);
			this.spawnCloud4.start();
			
		}
			this.spawnCloud3 = game.time.create(false);  
			this.spawnCloud3.loop(Phaser.Timer.SECOND * 0.38, spawnCloud, this, 3);
			this.spawnCloud3.start();
		
		
        
 		
		//creates teleport smoke asset
		this.smoke = game.add.sprite(-50, -50, 'pc', 'puff1');
		this.smoke.animations.add('puff', Phaser.Animation.generateFrameNames('puff', 1, 3, '', 1), 15, false);
		this.smoke.alpha = 0; 
		this.smoke.anchor.set(0.5);
		
		//starting cloud guaranteed to catch the player
		var startCloud = new Cloud(game, 'clouds', -800, 1);
		this.cloud.add(startCloud);  
		startCloud = new Cloud(game, 'clouds', -800, 2);
		this.cloud.add(startCloud);
		
		game.time.events.loop(Phaser.Timer.SECOND * 0.25, spawnParallax, this, 1);
		game.time.events.loop(Phaser.Timer.SECOND * 2, spawnParallax, this, 0);

	  	//Phase 3 large cloud stuff  
		tplcloudFront = game.add.sprite(1300, 320, 'tplcloudFront');
		tplcloudBack = game.add.sprite(1300, 270, 'tplcloudBack');
		lantern = game.add.sprite(1800, 320, 'temple', 'lantern');
		light = game.add.sprite(1830, 355, 'temple', 'light');
		light.anchor.set(0.5);
		game.physics.arcade.enable(tplcloudBack);
		game.physics.arcade.enable(tplcloudFront);
		game.physics.arcade.enable(lantern);
		game.physics.arcade.enable(light);
		tplcloudBack.body.friction.x = 0;
		tplcloudBack.body.friction.y = 0;
		tplcloudBack.body.immovable = true;
		tplcloudBack.body.setSize(3201, 600, 0, 250);
		tplcloudBack.body.checkCollision.left = false;
		tplcloudBack.body.checkCollision.right = false;
		tplcloudBack.body.checkCollision.down = false;

		if(backFrom3){
			backFrom3 = false;
		}

	    //keeps the clouds on the top layer
	    //game.world.sendToBack(this.tornado);
		game.world.sendToBack(this.tornado);
	    game.world.sendToBack(this.tcloudBack);
	    game.world.sendToBack(templeIn);
		game.world.sendToBack(light);
	    game.world.sendToBack(lantern);
	    game.world.sendToBack(tplcloudBack);
	    game.world.sendToBack(this.parallax);
	    game.world.bringToTop(this.player);
	    game.world.bringToTop(this.sccloud);
	    game.world.bringToTop(this.display);
	    game.world.bringToTop(this.cloud);
		game.world.bringToTop(this.foreground);
	    game.world.bringToTop(this.tcloudFront);
	    game.world.bringToTop(templeOut);
	    game.world.bringToTop(tplcloudFront);
		
		this.foreground.alpha = 1;
		this.sccloud.alpha = 1;
		this.display.alpha = 1;
		
		
		if(this.tutorial) {
			this.foreground.alpha = 0;
		    this.sccloud.alpha = 0;
			this.display.alpha = 0;
        }			
		
		
	},
	
	update: function() {
		
		if(this.flickering) {
			game.time.events.add(Phaser.Timer.SECOND * 0, flicker, this);
		    if(light.alpha > 0.4)
			   light.alpha -= 0.01;
		}
		else {
			game.time.events.add(Phaser.Timer.SECOND * 0, flicker, this);
		    light.scale.setTo(1, 1);
			if(light.alpha < 1)
			   light.alpha += 0.01;
		}
		
		this.tcloudFront.subAll('tilePosition.x', 3);
		this.tcloudBack.subAll('tilePosition.x', 2);
		game.physics.arcade.collide(this.player, floor);  
	
		//makes player stand on platforms
		game.physics.arcade.collide(this.player, this.cloud);
		game.physics.arcade.collide(this.player, tplcloudBack, disJumpTel, null, this);

		//linearizes object movement after first landing
		if (!this.started && this.player.body.touching.down) {
			this.player.body.angularVelocity = 0;       
			this.player.body.gravity.x = 0;
			this.player.body.velocity.x = 0;
			this.started = true;
			//resets parallax vertical velocities;
			this.parallax.setAll('body.velocity.y', 0);
			this.foreground.setAll('body.velocity.y', 0);
			this.cloud.setAll('body.velocity.y', 0);
		}

		//before the player first lands, keeps the background moving vertically
		if (!this.started) {
			this.parallax.setAll('body.velocity.y', -350);
			this.foreground.setAll('body.velocity.y', -350);
			this.cloud.setAll('body.velocity.y', -350);
		}

		//teleports the player a short distance
		if((game.input.activePointer.leftButton.isDown && !this.teleporting && !this.disable && this.counter2 >= 0 && this.counter > 1 && this.e) || this.egg) {
			this.e = false;
			game.time.events.add(Phaser.Timer.SECOND * 3, function(){this.e = true;} , this);
			this.player.body.gravity.y = 0;
			this.player.body.velocity.y = 0;
			this.destination = 50;
			this.smoke.x = this.player.x;
			this.smoke.y = this.player.y;
			this.smoke.angle = Math.random() * 360;
			this.smoke.alpha = 1;
			var music = game.sound.play('poof');
			this.smoke.animations.play('puff');
			this.grounded = false;
			this.teleporting = true;
			console.log('Teleport');
			this.player.alpha = 0;
			this.counter2 = 0;
			if(!this.egg) {
				game.time.events.add(Phaser.Timer.SECOND * 0.4, tpDust, this);
				game.time.events.add(Phaser.Timer.SECOND * 0.5, teleport, this); }
			else {
				game.time.events.add(Phaser.Timer.SECOND * 0.001, tpDust, this);
				game.time.events.add(Phaser.Timer.SECOND * 0.002, teleport, this);
			}
			this.egg = false;
		}
		
		//variable jump height with key input
		if(this.jump > 0 && this.input.keyboard.downDuration(Phaser.Keyboard.SPACEBAR, 200)) {
			this.jumping = true;
			this.dropState = false;
			this.grounded = false;
			console.log('Jump');
	        this.player.body.velocity.y = this.JUMP_SPEED;
			this.telecool = true;
		
			//INSERT ANIMATIONS HERE
			if(this.counter2 <= 1)
	            this.player.animations.play('jump');      //Jump 1
		    else if (this.counter2 == 2)
				this.player.animations.play('jump');      //Jump 2
			else if (this.counter2 == 3)
				this.player.animations.play('jump');   //Jump 3
			else
				this.player.animations.play('jump');       //Post teleport recharge
			//INSERT ANIMATIONS HERE
	    }
		
	    if(this.player.body.velocity.y > 50 && !this.dropState && this.started) {
	    	this.grounded = false;
	    	this.player.animations.play('drop');
	    }
		
	    //letting go of the UP key subtracts a jump
	    if(this.jumping && this.input.keyboard.upDuration(Phaser.Keyboard.SPACEBAR)) {
	    	this.jump = 0;
	    	this.jumping = false;
	    }
		
		if (this.player.body.touching.down && !this.jumping && !this.grounded) {
		    if(this.telecool) {
		       this.counter2 += 1;
			   console.log(this.counter2);
			}
		   var music = game.sound.play('land', 0.3);
		   this.telecool  = false;
		}
		
	    //resets values when player becomes grounded
	    if (this.player.body.touching.down) {
	    	this.player.angle = 0;
	    	console.log('Grounded');
			this.player.animations.play('run');
			
			if(!this.disable && this.counter > 0) 
	    	   this.jump = 1;
		    else 
			   this.jump = 0;
		   
		    if(this.counter < 2)
			   this.counter += 1;
		   
		    if(this.teleporting) {
		   	   this.teleporting = false;
			   console.log('Teleport Recharge Start');
			}
		   
		    this.teleporting = false;
	    	this.jumping   = false;
			this.grounded  = true;
			this.dropState = false;
	    }
		
	    //game over state: when the player's y value exceeds bounds
	    if (this.player.y > 700) {   
	        this.smoke.destroy();                                     //********************************************
			if(!this.tutorial)
			   game.state.start('End', false, false, this.player, this.tornado, this.parallax, this.foreground, this.cloud, this.display, this.sccloud, this.debris, this.tcloudFront, this.tcloudBack, null, null, null, 1, this.tut, this.tut2, this.tutorial);
		    else {
				this.egg = true;
				this.player.angle = 0;
			}
	    }

	    //alters player angle while player is dropping from teleporting
	    if (this.dropState) {
	    	this.player.angle += 1;
	    }
	    if (!this.dropState && this.started) {
	    	this.player.angle = 0;
	    }
		
	    // *First Phase to second phase Transit ----- 
	    if ((this.shiftPhase % 10 == 0 && this.shiftPhase > 0 && !this.MOVETONEXT && !this.next3 && !this.tutorial) || (this.tutorial && this.apple && this.input.keyboard.isDown(Phaser.Keyboard.X))) {
			
			if(this.tutorial) {
				this.apple = false;
			    game.add.tween(this.instructions).to( { x: -450, y: 100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.spc).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.space).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.msc).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.mouse).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);	
                this.sccloud.alpha = 1;
                this.display.alpha = 1;				
			}
			
			
			
	    	this.MOVETONEXT = true;
         
			// add tilesprites in front(x, y, width, height, key)
			var newtCloud = game.add.tileSprite(0, -400, 2106, 400, 'tCloud', 'tcloudUF');
			//newtCloud.alpha = 0.95;
			game.physics.arcade.enable(newtCloud);
			game.add.tween(newtCloud).to({y: -250}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.Out, true);
			this.tcloudFront.add(newtCloud);
			
			newtCloud = game.add.tileSprite(-300, 600, 2106, 400, 'tCloud', 'tcloudDF');
			//newtCloud.alpha = 0.95;
			game.physics.arcade.enable(newtCloud);
			var prePrep = game.add.tween(newtCloud).to({y: 410}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.Out, true);
			this.tcloudFront.add(newtCloud);

			// add tilesprites in back(x, y, width, height, key)
			newtCloud = game.add.tileSprite(0, -400, 2106, 400, 'tCloud', 'tcloudUB');
			newtCloud.alpha = 0.95;
			game.physics.arcade.enable(newtCloud);
			game.add.tween(newtCloud).to({y: -170}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.Out, true);
			this.tcloudBack.add(newtCloud);
			
			newtCloud = game.add.tileSprite(-300, 600, 2106, 400, 'tCloud', 'tcloudDB');
			newtCloud.alpha = 0.95;
			game.physics.arcade.enable(newtCloud);
			var prePrep = game.add.tween(newtCloud).to({y: 380}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.Out, true);
			this.tcloudBack.add(newtCloud); 

			prePrep.onComplete.add(prepare, this);
	    }
		 
	     // *First Phase to third phase Transit -----                                               //**********************************
	    if (this.shiftPhase % 10 == 0 && this.shiftPhase > 0 && !this.MOVETO3 && this.next3) {
	    	this.MOVETO3 = true;
	  
			tplcloudBack.body.velocity.x = -800;
			tplcloudFront.body.velocity.x = -800;
			templeOut.body.velocity.x = -800;
			lantern.body.velocity.x = -800;
			light.body.velocity.x = -800;
			templeIn.body.velocity.x = -800;
			game.world.bringToTop(this.foreground);
			game.time.events.add(Phaser.Timer.SECOND * 0.5, cdDestroy, this);
	
	    }

	    if (templeOut.x <= 400 && this.MOVETO3) {
	    	game.time.events.add(0, moveToThree, this);
	    }

		//scales tint in and out of dark clouds
	    this.tcloudFront.setAll('tint', (this.rgb * 0x10000) + (this.rgb * 0x100) + this.rgb);
	    this.tcloudBack.setAll('tint', (this.rgb * 0x10000) + (this.rgb * 0x100) + this.rgb);

	    // *SHIFTING PHASE and moving sccloud
	    // conditions to meet: RCSpawned is set to false (this is to make sure function is only fired once)
	    //					   MOVETONEXT boolean is set
	    //                     the player is grounded
	    //                     the player is below a certain height so that the transition is visually evident when playing
	    if (!this.RCSpawned && this.MOVETONEXT && this.grounded && this.player.y > 200 && this.windupDone){
	    	this.jump = 0;
	    	this.teleporting = true;
	    	console.log('Preparing to move...');
	    	//this.shiftPhase = 11;
	    	this.FIRSTPHASE = false;
	    	this.SHIFTINGPHASE = true;
	    	this.RCSpawned = true;

			var delay = game.add.tween(this.sccloud).to( { x: 235, y: this.player.y}, 1600, Phaser.Easing.Quadratic.Out, true);
			game.add.tween(this.display).to( { x: 330, y: this.player.y + 30}, 1600, Phaser.Easing.Quadratic.Out, true);
			game.time.events.add(1200, oneTwo, this);
			this.player.body.velocity.y = -500;
	    	this.player.body.velocity.x = 300;
	    	this.player.body.gravity.y = 800;
	    	this.cloud.setAll('body.checkCollision.up', false);
			//this.player.body.gravity.y = 0;
			//this.player.body.velocity.y = 0;
			this.player.animations.play('roll');
			if(!this.tutorial) {
		       this.spawnCloud1.destroy();
			   this.spawnCloud2.destroy();
			}
	    }
	    //***** PHASE 1 to 3
	    if (!this.RCSpawned && this.MOVETO3 && this.grounded && this.player.y > 200){
	    	//this.cloud.setAll('body.checkCollision.up', false);

	    	console.log('Preparing to move 3...');
	    	//this.shiftPhase = 11;
	    	this.FIRSTPHASE = false;
	    	this.SHIFTINGPHASE = true;
	    	this.RCSpawned = true;
	    	this.player.angle = 0;

			game.add.tween(this.sccloud).to({ x: 800}, 1600, Phaser.Easing.Quadratic.Out, true);
			game.add.tween(this.display).to({ x: 890}, 1600, Phaser.Easing.Quadratic.Out, true);
	    }

		//creates puff particles for teleporting
	if (this.smoke.alpha > 0) {
		var dust = new Puff(game, 'pc', this.smoke.x, this.smoke.y);
		game.add.existing(dust);
		this.smoke.alpha -= 0.05;
	}
		//toggles debug information
		if (this.input.keyboard.justPressed(Phaser.Keyboard.O)) {
		    this.debug = !this.debug;
		}

	},
	render: function() {
		if (this.debug) {
			game.debug.text('Jumping boolean: [' + this.jumping + '] Teleporting boolean: [' + this.teleporting + ']', 32, 80, "#fffffff");
			game.debug.text('asset count: ' + this.parallax.countLiving(this), 32, 96, "#fffffff");
			game.debug.text('Player: X = ' + this.player.x + ' Y = ' + this.player.y, 32, 112, "#fffffff");
			game.debug.body(this.sccloud);
			game.debug.cameraInfo(game.camera, 32, 256);
			game.debug.body(tplcloudBack);
		}
	}
}

//creates clouds and puts it onto the stage
function spawnCloud(flag) {
	if(Math.random() < this.difficulty) {
		var arrayCloud = [1, 1, 1, 1, 1, 2];
		var cloudList = ['c1', 'c2', 'c3', 'c4', 'c4', 'c5', 'c6', 'c5', 'c6', 'c7', 'c8', 'c9'];
		var e      = arrayCloud[game.rnd.integerInRange(0, arrayCloud.length)];
		var len    = arrayCloud[Math.floor(Math.random() * arrayCloud.length)];
		var xAxis  = game.rnd.integerInRange(game.width + 240, game.width + 241);
		var cloudA = Phaser.ArrayUtils.getRandomItem(cloudList);
		var cloudB = Phaser.ArrayUtils.getRandomItem(cloudList);
		var cloudC = Phaser.ArrayUtils.getRandomItem(cloudList);
		
		if (flag == 1) {
			var yAxis  = game.rnd.integerInRange(100, 130);
		} else if (flag == 2) {
			var yAxis  = game.rnd.integerInRange(190, 260);
		} else if (flag == 3){
			var yAxis  = game.rnd.integerInRange(390, 460);
		} else if (flag == 3){
			var yAxis  = game.rnd.integerInRange(540, 550);
		} else {
			var yAxis = 530;
		}
		
		if (len == 1){
			var newCloud = new Cloud(game, 'clouds', -800, 0, xAxis, yAxis, cloudA);
		} 
		else if (len == 2){
			var newCloud = new Cloud(game, 'clouds', -800, 0, xAxis, yAxis, cloudA);
			var newCloud1 = new Cloud(game, 'clouds', -800, 0, xAxis - 30 , yAxis, cloudB);
			game.add.existing(newCloud1);
			game.physics.arcade.enable(newCloud1);
			newCloud1.body.checkCollision.left = false;
			newCloud1.body.checkCollision.right = false;
			newCloud1.body.checkCollision.down = false;
			this.cloud.add(newCloud1);
		}
		/*
		else {
			var newCloud = new Cloud(game, 'clouds', -800, 0, xAxis, yAxis, cloudA);
			var newCloud1 = new Cloud(game, 'clouds', -800, 0, xAxis - 30, yAxis, cloudB);
			var newCloud2 = new Cloud(game, 'clouds', -800, 0, xAxis - 60, yAxis, cloudC);
			game.add.existing(newCloud1);
			game.physics.arcade.enable(newCloud1);
			newCloud1.body.checkCollision.left = false;
			newCloud1.body.checkCollision.right = false;
			newCloud1.body.checkCollision.down = false;
			this.cloud.add(newCloud1);
			game.add.existing(newCloud2);
			game.physics.arcade.enable(newCloud2);
			newCloud2.body.checkCollision.left = false;
			newCloud2.body.checkCollision.right = false;
			newCloud2.body.checkCollision.down = false;
			this.cloud.add(newCloud2);
		}
		*/
		game.add.existing(newCloud);
		game.physics.arcade.enable(newCloud);
		newCloud.body.checkCollision.left = false;
		newCloud.body.checkCollision.right = false;
		newCloud.body.checkCollision.down = false;
		this.cloud.add(newCloud);
		//console.log(yAxis);
	}
}

function starts() {
	this.apple = true;
}

function disJumpTel() {
   if(!this.disable) {	
      console.log('Jump & Teleport disabled');
      this.disable = true;
   }
}

function cdDestroy() {
	if(!this.tutorial) {
       this.spawnCloud1.destroy();
	   this.spawnCloud2.destroy();
    }
	this.spawnCloud3.destroy();
	this.spawnCloud4.destroy();	
}

//creates a smoke cloud and puts it at the player location
function tpDust() {
		var music = game.sound.play('poof');
		this.smoke.x = this.player.x;
		this.smoke.y = this.destination;
		this.smoke.angle = Math.random() * 360;
		this.smoke.alpha = 1;
		this.smoke.animations.play('puff');
}

//teleports the player back onto the stage by making them visible again
function teleport() {
	this.player.y = this.destination;
	this.dropState = true;
	if(!this.tutorial)
    	this.player.animations.play(Phaser.ArrayUtils.getRandomItem(this.tpState));
    else 
		this.player.animations.play('flail');
	this.player.alpha = 1;
	this.player.body.velocity.y = -50;
	this.player.body.gravity.y = 1800;
}

function flicker(){
	if(this.flickering)
		 game.time.events.add(Phaser.Timer.SECOND * 0.65, function(){this.flickering = false;}, this);
	else
	     game.time.events.add(Phaser.Timer.SECOND * 0.65, function(){this.flickering = true;}, this);
}

function timeScoreOof() {
	this.score2 += 1;
	if(this.score2 <= 1000)
       this.display.text = 'Distance: ' + this.score2 + 'm';
    else
	   this.display.text = 'Distance: ' + Math.floor(this.score2/1000) + '.' + Math.floor(((this.score2%1000)/100)%10) +  Math.floor(((this.score2%1000)/10)%10) + 'km';
   
   if(this.tutorial)
	   this.display.text = '    TUTORIAL';
}
//increments the score on a second interval, while also randomizing the cloud movement
function timeScore() {
	this.score += 1;
	this.shiftPhase += 1;
	
	if(this.FIRSTPHASE && this.backTime >= 3){
		var offset1 = Math.random() * 100;
		var offset2 = Math.random() * 100;
		game.add.tween(this.display).to( { x: 435 + offset1, y: 135 + offset2}, 1000, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.sccloud).to( { x: 345 + offset1, y: 105 + offset2}, 1000, Phaser.Easing.Quadratic.InOut, true);
	}
}

//sets boolean to true once tweens have completed
function oneTwo() {
	console.log('oneTwo triggered');
	this.player.body.gravity.y = 0;
	this.player.body.velocity.y = 0;
	this.player.body.velocity.x = 0;
	game.add.tween(this.player).to( { x: this.sccloud.x + 170, y: this.sccloud.y - 10}, 10, Phaser.Easing.Quadratic.In, true);
	game.time.events.add(10, moveToTwo, this);
	this.oneTwo = true;
}
//sets boolean to true once tweens have completed //****************************
function oneThree() {
	console.log('oneThree triggered');
	wall.setAll('body.checkCollision.none', false);
	floor.setAll('body.checkCollision.none', false);
	floor.setAll('body.checkCollision.up', true);
	floor.setAll('body.checkCollision.left', false);
	floor.setAll('body.checkCollision.right', false);
	floor.setAll('body.checkCollision.down', false);
	game.time.events.add(10, moveToThree, this);
	this.oneThree = true;
}

function moveToTwo() {
	this.smoke.destroy();
	this.player.angle = 0;
	game.state.start('Play2', false, false, this.player, this.tornado, this.parallax, this.foreground, this.cloud, this.display, this.sccloud, this.score, this.tcloudFront, this.tcloudBack, this.score2, this.tut, this.tut2, this.tutorial);
}

function moveToThree() {
	this.smoke.destroy();
	game.state.start('Play3', false, false, this.player, this.tornado, this.parallax, this.foreground, this.cloud, this.display, this.sccloud, this.score, this.mural, this.score2, this.tut, this.tut2, this.tutorial);
}

function prepare() {
	this.tornado.alpha = 1;
	var prep = this.tornado.animations.play('windup');
	//game.world.bringToTop(this.sccloud);
	//game.world.bringToTop(this.display);
	game.time.events.repeat(0.1, 50, tweenTint, this);
	prep.onComplete.add(windupComplete, this);
}

function windupComplete() {
	this.windupDone = true;
	this.tornado.animations.play('funnel');
	//game.world.bringToTop(this.sccloud);
	//game.world.bringToTop(this.display);
}

function tweenTint() {
    this.rgb--;
}