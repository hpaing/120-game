/*
120 - Final Project 
*/

"use strict";

var play3tutorial = false;

var Play3 = function(game){};
Play3.prototype = {
	init: function(player, tornado, parallax, foreground, cloud, display, sccloud, score, mural, score2, tut, tut2, tutorial) {
		this.player = player;
		this.tornado = tornado;
		this.parallax = parallax;
		this.foreground = foreground;
		this.cloud = cloud;
		this.display = display;
		this.sccloud = sccloud;
		this.score = score;
		this.score2 = score2;
		this.tpState = ['flex', 'chill', 'flail', 'magic', 'spin'];
		this.disable = false;		//boolean that fully disables all input when starting to transition back to Phase One
		this.play3tutorial = tut2;
		this.instructions;
        this.tut = tut;
		this.debug = false;
		this.entered  = false;

		this.cleanup = false;
		this.next3 = false;
		this.grounded = false;
		this.back1 = false;
		this.topped = false;
		this.readytop = false;
		this.ready = false;		//becomes true once the player is at the top and is 'idling'
		this.teleporting = false;
		this.tpCD = true;
		this.safetyOn = false;
		this.extra = true;
		this.f = false;
		this.tcloudFront;
		this.tcloudBack;

		this.sun;
		this.sunclouds;
		this.spawnCloud;
		this.spawnWind;
		
		this.tutorial = tutorial;
	},
	create: function() {
		//creates tclouds here as well
		this.tcloudFront = game.add.tileSprite(0, -250, 2106, 400, 'tCloud', 'tcloudUF');
		this.tcloudFront.alpha = 0.95;
		game.physics.arcade.enable(this.tcloudFront);
		this.tcloudBack = game.add.tileSprite(0, -300, 2106, 400, 'tCloud', 'tcloudUB');
		this.tcloudBack.alpha = 0.95;
		game.physics.arcade.enable(this.tcloudBack);
		
		if(this.play3tutorial)
			this.f = true;

		//creates continue prompt
		this.continue = game.add.sprite(300, 500, 'misc', 'continue');
		this.continue.alpha = 0;

		//determines instructions
		if (!this.play3tutorial) {
			this.instructions = game.add.sprite(400, 2550, 'misc', 'inst3');
			this.play3tutorial = true;
		}
		else {
			this.instructions = game.add.sprite(400, 2550, 'misc', 'badinst');
		}
		this.instructions.anchor.setTo(0.5);

		//pre-creates scenery
		this.sun = game.add.sprite(700, 0, 'sunery', 'sun');
		this.sun.scale.setTo(1.5, 1.5);
		this.sun.anchor.setTo(0.5);
		game.physics.arcade.enable(this.sun);
		this.sunclouds = game.add.group();

		//creates the mural
		var mural = ['mural1', 'mural2', 'mural3', 'mural4'];
		this.mural = game.add.sprite(templeIn.x, 1025, Phaser.ArrayUtils.getRandomItem(mural));
		this.mural.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.mural);

		//physics stuff
		baseFloor = game.add.graphics(0, 0);
		baseFloor.beginFill(0x000000);
		baseFloor.drawRect(0, 0, 576, 20);
		baseFloor.endFill();
		game.physics.arcade.enable(baseFloor);
		baseFloor.x = 100;
		baseFloor.y = 2670;
		floor.add(baseFloor);
		baseFloor.alpha = 0;

		topFloor = game.add.graphics(0, 0);
		topFloor.beginFill(0x000000);
		topFloor.drawRect(0, 0, 576, 20);
		topFloor.endFill();
		game.physics.arcade.enable(topFloor);
		topFloor.x = 100;
		topFloor.y = 512;
		floor.add(topFloor);
		topFloor.alpha = 0;

		wallL = game.add.graphics(0, 0);
		wallL.beginFill(0x000000);
		wallL.drawRect(0, 0, 20, 2560);
		wallL.endFill();
		game.physics.arcade.enable(wallL);
		wallL.x = 80;
		wallL.y = 128;
		wall.add(wallL);
		wallL.alpha = 0;

		wallR = game.add.graphics(0, 0);
		wallR.beginFill(0x000000);
		wallR.drawRect(0, 0, 20, 2200);
		wallR.endFill();
		game.physics.arcade.enable(wallR);
		wallR.x = 672;
		wallR.y = 512;
		wall.add(wallR);
		wallR.alpha = 0;

		invisDoor = game.add.graphics(0, 0);
		invisDoor.beginFill(0x64B5E7);
		invisDoor.drawRect(0, 0, 20, 330);
		invisDoor.endFill();
		invisDoor.alpha = 0;
		game.physics.arcade.enable(invisDoor);
		invisDoor.body.immovable = true;
		invisDoor.x = 687;
		invisDoor.y = 180;

		game.time.events.add(50, entry, this);

		// assets floor generation
		var platformList = ['platform1', 'platform1', 'platform2', 'platform2', 'platform2', 'platform3'];
		for(var k = 2500; k > 450; k -= 150){
			console.log('created!');
			var platform = Phaser.ArrayUtils.getRandomItem(platformList);
			var newFloor = game.add.sprite(1160, k+20, 'temple', platform);
			newFloor.anchor.setTo(0.5);
			game.physics.arcade.enable(newFloor);
			if(platform == 'platform1'){
				// Change hitbox
				// body.setSize(width, height [, offsetX] [, offsetY])
				newFloor.body.setSize(142, 8, 1, 60);
				// *Randomize x coordinate
				newFloor.x = game.rnd.integerInRange(190, 610);
			}
			else if (platform == 'platform2'){
				// Change hitbox
				// body.setSize(width, height [, offsetX] [, offsetY])
				newFloor.body.setSize(256, 8, 1, 60);
				// *Randomize x coordinate
				newFloor.x = game.rnd.integerInRange(150, 550);
			}
			else if (platform == 'platform3'){
				// Change hitbox
				// body.setSize(width, height [, offsetX] [, offsetY])
				newFloor.body.setSize(370, 8, 1, 60);
				// *Randomize x coordinate
				newFloor.x = game.rnd.integerInRange(300, 500);
			}

			floor.add(newFloor);
		}
	    
	    wall.setAll('body.checkCollision.none', false);
		floor.setAll('body.checkCollision.none', false);
		floor.setAll('body.checkCollision.up', true);
		floor.setAll('body.checkCollision.left', false);
		floor.setAll('body.checkCollision.right', false);
		floor.setAll('body.checkCollision.down', false);
		wall.setAll('body.immovable', true);
		floor.setAll('body.immovable', true);

		console.log('Entered [Play3]');
		game.add.tween(templeOut).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
		game.add.tween(tplcloudFront).to({alpha: 0}, 500, Phaser.Easing.Linear.None, true);
		// keeps the parallax running in this state
		// game.time.events.loop(Phaser.Timer.SECOND * 0.25, spawnParallax, this, this.parallax, -200);
		// game.time.events.loop(Phaser.Timer.SECOND * 2, spawnParallax, this, this.foreground, 0);

		// keeps the timer going in this state as well
		// game.time.events.loop(Phaser.Timer.SECOND, timeScore2, this);
		
	    this.player.body.immovable = false;

    	// Change the player movements & animation
	    this.player.animations.play('run');
	    this.player.body.gravity.y = 1800;

	    // Set new coordinates for everything in the world.
	    this.parallax.addAll('y', 2150);
	    this.foreground.addAll('y', 2150);
	    this.cloud.addAll('y', 2150);
	    this.player.y += 2150;
	    templeIn.y += 2150;
	    templeOut.y += 2150;
	    tplcloudFront.y += 2150;
	    tplcloudBack.y += 2150;
	    game.camera.y = 2150;
	    tplcloudFront.body.velocity.x = 0;
	    tplcloudBack.body.velocity.x = 0;
	    templeIn.body.velocity.x = 0;
	    templeOut.body.velocity.x = 0;
	    game.time.events.add(Phaser.Timer.SECOND * 0.4, function(){game.camera.follow(this.player, Phaser.Camera.FOLLOW_PLATFORMER, 0.5, 0.5);}, this);
	    game.camera.deadzone = new Phaser.Rectangle(300, 200, 200, 300);
	    // create a rectangle sized to the dead zone (for debug purposes)
		// we need a null check in case the Phaser camera style is FOLLOW_LOCKON
		if(game.camera.deadzone !== null) {
			this.zx = game.camera.deadzone.x;
			this.zy = game.camera.deadzone.y;
			this.zone = new Phaser.Rectangle(this.zx, this.zy, game.camera.deadzone.width, game.camera.deadzone.height);
		}

		//"preloads" scenery

		game.world.bringToTop(floor);
		game.world.bringToTop(templeOut);
		game.world.bringToTop(tplcloudFront);
		game.world.bringToTop(this.foreground);
		game.world.bringToTop(this.tcloudFront);
		game.world.bringToTop(this.continue);
	    game.world.sendToBack(this.mural);
	    game.world.sendToBack(this.tornado);
	    game.world.sendToBack(this.instructions);
	    game.world.sendToBack(templeIn);
	    game.world.sendToBack(tplcloudBack);
	    game.world.sendToBack(this.tcloudBack);
	    game.world.sendToBack(this.parallax);
	    game.world.sendToBack(this.sunclouds);
	    game.world.sendToBack(this.sun);

	    // *Force movement
	    this.player.angle = 0;
	    this.player.body.velocity.x = 600;
	    floor.setAll('body.velocity.x',100);
	    baseFloor.body.velocity.x = 0;
	    topFloor.body.velocity.x = 0;

	    invisDoor.body.checkCollision.none = false;

	    //creates teleport smoke asset
		this.smoke = game.add.sprite(-50, -50, 'pc', 'puff1');
		this.smoke.animations.add('puff', Phaser.Animation.generateFrameNames('puff', 1, 3, '', 1), 15, false);
		this.smoke.alpha = 0;
		this.smoke.anchor.set(0.5);

		//parallax test
		game.time.events.loop(Phaser.Timer.SECOND * 0.25, spawnParallax, this, 3);
	},
	update: function() {
		// *Third Phase mechanics--------------------
		game.physics.arcade.collide(this.player, floor);

		this.tcloudFront.tilePosition.x -= 0.5;
		this.tcloudBack.tilePosition.x -= 0.25;
		//this.tcloudBack.subAll('tilePosition.x', 2);

		//teleports the player a short distance
		if(!this.readytop && game.input.activePointer.leftButton.isDown && !this.teleporting && this.tpCD) {
			this.teleporting = true;
			this.tpCD = false;
			this.player.body.gravity.y = 0;
			this.player.body.velocity.y = 0;
			this.player.body.velocity.x = 0;
			this.smoke.x = this.player.x;
			this.smoke.y = this.player.y;
			this.smoke.angle = Math.random() * 360;
			this.smoke.alpha = 1; 
			var music = game.sound.play('poof');
			this.smoke.animations.play('puff');
			this.grounded = false;
			this.teleporting = true;
			console.log('Teleport3');
			this.player.alpha = 0;

			game.add.tween(this.player).to( { y: this.player.y - 170}, Phaser.Timer.SECOND * 0.4, Phaser.Easing.Quadratic.InOut, true);
			game.time.events.add(Phaser.Timer.SECOND * 0.4, tpDust3, this);
			game.time.events.add(Phaser.Timer.SECOND * 0.4, teleport3, this);+
			game.time.events.add(Phaser.Timer.SECOND , function(){this.tpCD = true;}, this);
		}
			//creates puff particles for teleporting
		if (this.smoke.alpha > 0) {
			var dust = new Puff(game, 'pc', this.smoke.x, this.smoke.y);
			game.add.existing(dust);
			this.smoke.alpha -= 0.05;
		}

		
		if (!this.readytop && this.input.keyboard.justPressed(Phaser.Keyboard.SPACEBAR) && !this.disable && this.grounded && !this.teleporting) {
		    this.grounded = false;
		    this.player.body.velocity.y = -800;
		    console.log('Jump-3');
		    this.player.animations.play('leap');
		    this.grounded = false;
		}
		
		if (this.input.keyboard.isDown(Phaser.Keyboard.UP)){
			game.camera.y -=80;
		}
		
		if (this.input.keyboard.isDown(Phaser.Keyboard.DOWN)){
			game.camera.y +=80;
		}
		
		if (this.player.body.velocity.y > 50) {
	    	this.grounded = false;
	    	if (!this.ready) {
	    		this.player.animations.play('fall');
	    	}
	    }
		
	    //resets values when player becomes grounded
	    if (this.player.body.touching.down && this.player.body.velocity.y >=0 && !this.grounded) {
	    	var music = game.sound.play('land', 0.3);
	    	console.log('Grounded-3');
			this.player.animations.play('run');
			this.grounded = true;
			this.teleporting = false;
	    }
	    if (this.entered) {
		    // movement test
			game.physics.arcade.collide(this.player, wall, hitWall, null, this);
		    // floor reverse
		    game.physics.arcade.overlap(floor, wall, floorRev, null, this);
		}

		if (this.input.keyboard.justPressed(Phaser.Keyboard.O)) {
		    this.debug = !this.debug;
		}

		if (this.topped && this.grounded) {
	    	game.physics.arcade.collide(this.player, invisDoor, exitTemple, null, this);
		}
		else {
			game.physics.arcade.collide(this.player, invisDoor, leftBack, null, this);
		}

		if(this.player.y<=477){
			this.readytop = true;
			console.log('ReadyTop, safety off');
		} 
		
		if(this.readytop)
			this.safetyOn = false;
		else {
			this.safetyOn = true;
        }		
		
		if(this.readytop && this.grounded){
			this.topped = true;
		}//!(this.player.body.touching.left || this.player.body.touching.right)
		if(!this.disable && this.topped && this.player.body.velocity.x == 0){
			this.player.scale.x = 1;
			this.player.body.velocity.x = 300;
		}

		if (this.ready && this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.extra && this.f) {
			this.spawnWind.stop();
			this.spawnCloud.stop();
			game.add.tween(this.continue).to( {alpha: 0}, 1000, Phaser.Easing.Linear.None, true);
			this.extra = false;
			templeIn.body.acceleration.x = -800;
			templeOut.body.acceleration.x = -800;
			this.player.body.velocity.y = -600;
			this.player.body.gravity.y = 600;
			this.player.animations.play('roll');
			game.add.tween(this.player).to({x: 50}, 1000, Phaser.Easing.Quadratic.Out, true);
			game.time.events.add(800, dive, this);
			game.time.events.add(2000, threeOne, this);
		}

		// * Wall Safety
		if(this.safetyOn && this.player.x <= 140){
			//this.player.x = 145;
			this.player.scale.x = 1;
			this.player.body.velocity.x = 600;
		}
		if(this.safetyOn && this.player.x >=633){
			//this.player.x = 630;
			this.player.scale.x = -1;
			this.player.body.velocity.x = -600;
		}
	},
	render: function() {
		if (this.debug) {
			game.debug.cameraInfo(game.camera, 32, 32);
			game.debug.body(this.player);
			//game.debug.body(invisDoor);
			game.debug.text('Player: X = ' + this.player.x + ' Y = ' + this.player.y, 32, 300, "#fffffff");
			game.debug.text('this.tele = ' + this.teleporting + ' this.grounded ' + this.grounded, 300, 32, "#fffffff");
			game.debug.physicsGroup(floor);
			game.debug.physicsGroup(wall);
			//game.debug.geom(this.zone, 'rgba(255,0,0,0.25');
		}
	}
}

//will be updated with dive animation
function dive() {
	this.player.animations.play('dive');
	this.sun.body.gravity.y = -1200;
	this.sunclouds.setAll('body.gravity.y', -1300);
	this.tcloudFront.body.gravity.y = -1800;
	this.tcloudBack.body.gravity.y = -1800;
	game.add.tween(this.player).to({y: 100}, 1000, Phaser.Easing.Quadratic.Out, true);
	this.player.body.gravity.y = 0;
	this.player.body.velocity.y = 0;
}

function threeOne(){
	backFrom3 = true;
	this.instructions.destroy();
	this.continue.destroy();
	this.sun.destroy();
	this.tcloudFront.destroy();
	this.tcloudBack.destroy();
	this.sunclouds.destroy(true);
	tplcloudFront.destroy();
	tplcloudBack.destroy();
	floor.removeAll(true);
	wall.removeAll(true);
	this.smoke.destroy();
	game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, this.score2, this.tut, this.play3tutorial, this.tutorial);
}

function exitTemple(){
	invisDoor.body.checkCollision.none = true;
	floor.setAll('body.immovable', true);
	wall.setAll('body.immovable', true);

	this.player.body.velocity.x = 0;
	this.player.body.velocity.y = 0;
	this.player.body.gravity.y = 0;

	this.back1 = true;
	this.disable = true;
	floor.setAll('body.velocity.x', 0);
	floor.setAll('body.acceleration.x', -500);
	invisDoor.destroy();
	wall.setAll('body.acceleration.x', -500);
	// templeIn.body.acceleration.x = -500;
	// templeOut.body.acceleration.x = -500;

	floor.forEach(function(child){
		game.add.tween(child).to( {alpha: 0}, 750, Phaser.Easing.Linear.None, true);
	}, this);
	game.add.tween(templeIn).to( {x: templeIn.x - 700}, 1500, Phaser.Easing.Linear.None, true);
	game.add.tween(this.tcloudFront).to( {x: this.tcloudFront.x - 700}, 1500, Phaser.Easing.Linear.None, true);
	game.add.tween(this.tcloudBack).to( {x: this.tcloudBack.x - 700}, 1500, Phaser.Easing.Linear.None, true);
	game.add.tween(this.mural).to( {x: this.mural.x - 700}, 1500, Phaser.Easing.Linear.None, true);
	game.add.tween(templeOut).to( {x: templeOut.x - 700}, 1500, Phaser.Easing.Linear.None, true);

	var wait = game.add.tween(templeOut).to({alpha: 1}, 500, Phaser.Easing.Linear.None, true);
	wait.onComplete.add(jumpUp, this);
	//TEMPLE.setAllChildren('body.velocity.x', 0);
	//TEMPLE.setAllChildren('body.acceleration.x', -500);


	this.player.scale.x = 1;
	this.sccloud.x =845;
	this.sccloud.y =355;
	this.display.x =950;
	this.display.y =385;
	//game.add.tween(this.display).to({ x: 800}, 1600, Phaser.Easing.Quadratic.Out, true);
	//game.add.tween(this.display).to( { x: 500, y: 185 }, 1000, Phaser.Easing.Quadratic.InOut, true);
	//game.add.tween(this.sccloud).to( { x: 395, y: 155 }, 1000, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(game.camera).to( { y: 150 }, 1000, Phaser.Easing.Quadratic.InOut, true);
}
function jumpUp() {
	tplcloudFront.destroy();
	tplcloudBack.destroy();
	lantern.destroy();
	light.destroy();
	this.mural.destroy();
	this.player.body.velocity.y = -400;
	this.player.body.gravity.y = 900;
	game.time.events.add(500, tweenPlayer, this);
	game.time.events.add(1000, positionPlayer, this);
	this.player.animations.play('roll');
}
function tweenPlayer() {
	game.add.tween(this.player).to({y: 520}, 490, Phaser.Easing.Quadratic.InOut, true);
}
function positionPlayer() {
	//this.player.x = 700;
	//this.player.y = 550;
	this.player.body.gravity.y = 0;
	this.player.body.velocity.y = 0;
	game.add.tween(this.player).to({x: this.player.x - 400}, 860, Phaser.Easing.Quadratic.Out, true);
	game.add.tween(templeIn).to({x: templeIn.x - 400}, 860, Phaser.Easing.Quadratic.Out, true);
	game.add.tween(this.tcloudFront).to( {x: this.tcloudFront.x - 400}, 860, Phaser.Easing.Quadratic.Out, true);
	game.add.tween(this.tcloudBack).to( {x: this.tcloudBack.x - 400}, 860, Phaser.Easing.Quadratic.Out, true);
	var jump = game.add.tween(templeOut).to({x: templeOut.x - 400}, 860, Phaser.Easing.Quadratic.Out, true);
	this.player.animations.play('crouch');
	jump.onComplete.add(leap, this);
}

function leap() {
	var hoist = this.player.animations.play('launch');
	hoist.onComplete.add(spin, this);
}

function spin() {
	game.add.tween(templeIn).to({y: templeIn.y + 860}, 860, Phaser.Easing.Quadratic.Out, true);
	game.add.tween(this.tcloudFront).to( {y: this.tcloudFront.y + 860}, 860, Phaser.Easing.Quadratic.Out, true);
	game.add.tween(this.tcloudBack).to( {y: this.tcloudBack.y + 860}, 860, Phaser.Easing.Quadratic.Out, true);
	game.add.tween(this.sun).to( {y: this.sun.y + 650}, 860, Phaser.Easing.Quadratic.Out, true);
	var drop = game.add.tween(templeOut).to({y: templeOut.y + 860}, 860, Phaser.Easing.Quadratic.Out, true);
	this.player.body.velocity.x = 700;
	this.player.body.gravity.x = -1350;
	this.player.animations.play('backflip');
	drop.onComplete.add(goBack, this);
}
function goBack() {
	this.player.body.velocity.x = 0;
	this.player.body.gravity.x = 0;
	game.add.tween(this.player).to( {x: 238, y: 535}, 120, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(templeIn).to({y: templeIn.y - 120}, 120, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(this.tcloudFront).to( {y: this.tcloudFront.y - 120}, 120, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(this.tcloudBack).to( {y: this.tcloudBack.y - 120}, 120, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(this.sun).to( {y: this.sun.y - 120}, 120, Phaser.Easing.Quadratic.InOut, true);
	var drop = game.add.tween(templeOut).to({y: templeOut.y - 120}, 120, Phaser.Easing.Quadratic.InOut, true);
	drop.onComplete.add(pitStop, this);
}
function pitStop() {
	var delay = this.player.animations.play('crouch');
	game.time.events.add(50, idleTime, this);
	//delay.onLoop.add(idleTime, this);
}
function idleTime() {
	this.spawnCloud = game.time.create(false);
	this.spawnCloud.loop(Phaser.Timer.SECOND * 2, spawnSunCloud, this);
	this.spawnCloud.start();
	this.spawnWind = game.time.create(false);
	this.spawnWind.loop(Phaser.Timer.SECOND * 0.2, spawnWinds, this);
	this.spawnWind.start();
	var delay = this.player.animations.play('swirl');
	delay.onComplete.add(idling, this);
}
function idling() {
	this.player.animations.play('idle');
	this.ready = true;
	game.time.events.add(Phaser.Timer.SECOND * 7, prompt, this);
}
function prompt() {
	if (this.extra) {
		this.f = true;
		game.add.tween(this.continue).to( {alpha: 1}, 1000, Phaser.Easing.Linear.None, true);
	}
}

function hitWall(player, wallins){
	if(!this.disable){
		if((this.player.body.wasTouching.left || this.player.body.touching.left ) && this.player.x >= wallins.x){
			console.log('hitleft');
			this.player.scale.x = 1;
			this.player.body.velocity.x = 600;
		}
		else if ((this.player.body.wasTouching.right || this.player.body.touching.right) && this.player.x <= wallins.x){
			console.log('hitright');
			this.player.scale.x = -1;
			this.player.body.velocity.x = -600;
			if(!this.safetyOn){
				console.log('Safety On');
				this.safetyOn = true;
			}	
		}
	}

}

function floorRev(floorins, wallins){
	if(!this.disable){
		if ((floorins.body.velocity.x > 0 && floorins.x <= wallins.x) || (floorins.body.velocity.x < 0 && floorins.x >= wallins.x)){
			floorins.body.velocity.x *= -1;
		}
	}
}

function entry() {
	this.entered = true;
}

//creates a smoke cloud and puts it at the player location
function tpDust3() {
		var music = game.sound.play('poof');
		this.smoke.x = this.player.x;
		this.smoke.y = this.player.y;
		this.smoke.angle = Math.random() * 360;
		this.smoke.alpha = 1;
		this.smoke.animations.play('puff');
}

//teleports the player back onto the stage by making them visible again
function teleport3() {
	//this.player.y = this.player.y - 170;
	this.player.animations.play('fall');
	this.player.alpha = 1;
	this.player.body.gravity.y = 1800;
	this.player.body.velocity.y = -5;
	if(this.player.scale.x == 1)
		this.player.body.velocity.x = 600;
	else
		this.player.body.velocity.x = -600;
}

function leftBack(){
	this.player.scale.x = -1;
	this.player.body.velocity.x = -600;
}

function spawnWinds() {
	var type = game.rnd.integerInRange(0, 1);
	var windbreeze;
	if (type == 0) {
		//spawn wind
		windbreeze = game.add.sprite(game.rnd.integerInRange(50, 750), game.rnd.integerInRange(200, 400), 'sunery', 'sunwind01');
		windbreeze.animations.add('flow', Phaser.Animation.generateFrameNames('sunwind', 1, 13, '', 2), 20, false);
	}
	else {
		//spawn breeze
		windbreeze = game.add.sprite(game.rnd.integerInRange(50, 750), game.rnd.integerInRange(200, 400), 'sunery', 'sunbreeze01');
		windbreeze.animations.add('flow', Phaser.Animation.generateFrameNames('sunbreeze', 1, 11, '', 2), 20, false);
	}
	var scaling = 0.4 + (Math.random() * 0.6);
	windbreeze.alpha = scaling;
	windbreeze.scale.setTo(scaling, scaling);
	windbreeze.anchor.setTo(0.5);
	var flip = game.rnd.integerInRange(0, 1);
	if (flip == 0) {
		windbreeze.scale.y *= -1;
	}
	var blow = windbreeze.animations.play('flow');
	game.physics.arcade.enable(windbreeze);
	windbreeze.body.velocity.x = -100;
	this.sunclouds.add(windbreeze);
	blow.onComplete.add(destroyWindbreeze, this, windbreeze);
}

function destroyWindbreeze(windbreeze) {
	windbreeze.destroy();
}

function spawnSunCloud() {
	var suncloudlist = ['suncloud1', 'suncloud2', 'suncloud3', 'suncloud4', 'suncloud5', 'suncloud6'];
	//var newsuncloud = game.add.sprite(900, 150, 'sunery', 'sun');
	var newsuncloud = game.add.sprite(800, game.rnd.integerInRange(250, 450), 'sunery', Phaser.ArrayUtils.getRandomItem(suncloudlist));
	game.physics.arcade.enable(newsuncloud);
	newsuncloud.body.velocity.x = -50 - (Math.random() * 50);
	this.sunclouds.add(newsuncloud);
}