/*
120 - Final Project 
*/

"use strict";

var End = function(game){};
End.prototype = {
	//copies over score from previous state
	init: function(player, tornado, parallax, foreground, cloud, display, sccloud, debris, tcloudFront, tcloudBack, crowsBack, crows, streamlines, backstate, tut, tut2, tutorial) {
		this.player = player;
		this.tornado = tornado;
		this.parallax = parallax;
		this.foreground = foreground;
		this.cloud = cloud;
		this.display = display;
		this.sccloud = sccloud;
		this.ready = false;
		this.triggered = false;
		this.prompt;
		this.debris = debris;
		this.tcloudFront = tcloudFront;
		this.tcloudBack = tcloudBack;
		this.crowsBack = crowsBack;
		this.crows = crows;
		this.streamlines = streamlines;
		this.next3 = false;      //***********************************************
		this.backstate = backstate;
		this.reset = false;
		this.tut = tut;
		this.tut2 = tut2;
		this.tutorial = tutorial;
	}, 
	create: function() {

		this.player.body.immovable = false;
		this.player.alpha = 1;
		this.player.y = 800;            //**********************************************
		this.player.body.velocity.x = 0;

		//sends tornado away
		this.tornado.body.velocity.x = -400;
		this.tornado.body.velocity.y = -600;

		// *sends tcloud away
		this.tcloudFront.setAll('body.velocity.y', -600);
		this.tcloudBack.setAll('body.velocity.y', -600);

		// *Sends temple stuff away
		templeIn.body.velocity.y = -600;
		templeOut.body.velocity.y = -600;
		tplcloudFront.body.velocity.y = -600;
		tplcloudBack.body.velocity.y = -600;
		lantern.body.velocity.y = -600;
		light.body.velocity.y = -600;


		console.log('Entered [End]');
		//prints final score text
		this.prompt = game.add.sprite(game.width + 500, 250, 'misc', 'endprompt');
		game.time.events.add(Phaser.Timer.SECOND * 0.5, tweenprompt, this);
		game.add.tween(this.display).to( {fontSize: '22px', x: 270, y: 158}, 1500, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.sccloud).to({x: 100, y: 90}, 1500, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.sccloud.scale).to( {x: 1, y: 1}, 1500, Phaser.Easing.Quadratic.InOut, true);
		game.add.tween(this.player).to({x: 50}, 2000, Phaser.Easing.Quadratic.InOut, true);
		if(this.tutorial)
			this.prompt.destroy();
		//var instructions = game.add.text(16, 16, 'Game Over\nFinal Score: ' + this.score + '\nPress [Space] to Retry', {fontSize: '32px', fill: '#fff'});

		game.time.events.add(Phaser.Timer.SECOND * 2, ready, this);

		//creates parallax time interval
		game.time.events.loop(Phaser.Timer.SECOND * 0.2, spawnParallaxUp, this, this.parallax, 1);
		game.world.sendToBack(this.parallax);
		game.time.events.loop(Phaser.Timer.SECOND * 1, spawnParallaxUp, this, this.foreground, 0);
		game.world.bringToTop(this.foreground);

		//crow stuff
		if (this.crowsBack != null) {
			this.crowsBack.setAll('body.velocity.y', (-200));
			this.crows.setAll('body.velocity.x', (-100));
		}
		if (this.crows != null) {
			this.crows.setAll('body.velocity.y', (-200));
			this.crows.setAll('body.velocity.x', (-800));
		}
		if (this.streamlines != null) {
			this.streamlines.setAll('body.velocity.y', (-200));
			this.streamlines.setAll('body.velocity.x', (-800));
		}

		this.player.body.gravity.y = 0;
		this.player.body.velocity.y = -200;
		
		game.time.events.add(Phaser.Timer.SECOND * 20, function(){this.reset = true;} , this);
	},
	update: function() {
		//keeps the tclouds scrolling
		this.tcloudFront.subAll('tilePosition.x', 3);

		if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.ready && ! this.triggered) {
			
			this.triggered = true;
			this.ready = false;
			var delay = game.add.tween(this.display).to( { x: -35, y: -150}, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.sccloud).to({x: -200, y: -210}, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.prompt).to({x: -500, y: 0}, 1500, Phaser.Easing.Quadratic.InOut, true);
			delay.onComplete.add(replay, this);
		}
		
		if (((this.reset || game.input.keyboard.isDown(Phaser.Keyboard.M)) && this.ready && !this.triggered) || this.tutorial) {
			this.tutorial = false;
			this.triggered = true;
			this.ready = false;
			this.player.animations.play('drop');
			this.player.angle = 0;

			this.player.rotation = 0;
			this.player.body.angularVelocity = 0;

			game.time.events.add(Phaser.Timer.SECOND * 3, function(){this.ready = true;} , this);
			var delay = game.add.tween(this.display).to( { x: -35, y: -150}, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.sccloud).to({x: -200, y: -210}, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.prompt).to({x: -500, y: 0}, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			this.credits = game.add.sprite(game.width + 700, game.height + 200, 'misc', 'name');
			game.add.tween(this.credits).to ( { x: 570, y: 450 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			
		    this.gamename = game.add.sprite(game.width + 500, 600 - 400, 'misc', 'title');
			this.spc = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'spc1');
			this.spc.animations.add('spacecloud', Phaser.Animation.generateFrameNames('spc', 1, 2, '', 1), 20, true);
			this.spc.animations.play('spacecloud');
			this.spc.anchor.set(0.5);
			this.space = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'spacebar1');
			this.space.animations.add('press', Phaser.Animation.generateFrameNames('spacebar', 1, 2, '', 1), 3, true);
			this.space.animations.play('press');
			this.space.anchor.set(0.5);
			this.msc = game.add.sprite(game.width + 700, 600 - 200, 'misc', 'msc1');
			this.msc.animations.add('mousecloud', Phaser.Animation.generateFrameNames('msc', 1, 2, '', 1), 20, true);
			this.msc.animations.play('mousecloud');
			this.msc.anchor.set(0.5);
			this.mouse = game.add.sprite(game.width + 700, 600 - 200, 'misc', 'mouse1');
			this.mouse.animations.add('click', Phaser.Animation.generateFrameNames('mouse', 1, 2, '', 1), 3, true);
			this.mouse.animations.play('click');
			this.mouse.anchor.set(0.5);
			this.instructions = game.add.sprite(game.width + 500, 600 - 200, 'misc', 'inst');
			//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
			//this.credits = game.add.sprite(game.width + 700, game.height + 200, 'misc', 'name');
			//tweens title assets in
			var delay = game.add.tween(this.gamename).to( { x: 200, y: 90 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.spc).to( { x: 350, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		    game.add.tween(this.space).to ( { x: 350, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: 550, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to ( { x: 550, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			var delay = game.add.tween(this.instructions).to ( { x: 250, y: 300 }, 3000, Phaser.Easing.Quadratic.InOut, true);
		}
		
		if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.ready && this.triggered) {
			this.reset = false;
			this.ready = false;
			var delay = game.add.tween(this.gamename).to( { x: -400, y: -550 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			game.add.tween(this.instructions).to( { x: -450, y: 100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.spc).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.space).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			game.add.tween(this.credits).to ( { x: -400, y: 300 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			delay.onComplete.add(replay2, this);
		}
		/*
		if (game.input.keyboard.isDown(Phaser.Keyboard.X) && this.ready && this.triggered) {
			this.reset = false;
			this.ready = false;
			var delay = game.add.tween(this.gamename).to( { x: -400, y: -550 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			game.add.tween(this.instructions).to( { x: -450, y: 100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.spc).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.space).to( { x: -800, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to( { x: -400, y: -100 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			
			game.add.tween(this.credits).to ( { x: -400, y: 300 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			delay.onComplete.add(replay3, this);
		}
		*/
		if (this.player.y <= 250) {
			this.player.body.velocity.y = 0;
		}
		this.parallax.setAll('body.velocity.y', (this.player.body.velocity.y * 1) - 120);
		this.foreground.setAll('body.velocity.y', (this.player.body.velocity.y * 1) - 120);
		this.cloud.setAll('body.velocity.y', (this.player.body.velocity.y * 1) - 120);
		if(this.debris != null){
			this.debris.setAll('body.velocity.y', (this.player.body.velocity.y * 1) - 120);
		}
	}
}

//sets the ready boolean to true once the tweens have completed and the player is of reasonable height to not die immediately
function ready() {
	console.log('ready set');
	this.ready = true;
}

//cleans up unnecessary assets and moves on to the Play state
function replay() {
	this.display.destroy();
	this.sccloud.destroy();
	this.prompt.destroy();
	this.tornado.destroy();
	this.tcloudFront.destroy(true);
	this.tcloudBack.destroy(true);

	tplcloudFront.destroy();
	tplcloudBack.destroy();
	lantern.destroy();
	light.destroy();

	if (this.crowsBack != null) {
		this.crowsBack.destroy(true);
	}
	if (this.crows != null) {
		this.crows.destroy(true);
	}
	if (this.streamlines != null) {
		this.streamlines.destroy(true);
	}
	//creates new scorecloud with refreshed values
	this.sccloud = game.add.sprite(845, 355, 'misc', 'sc1');
	this.sccloud.alpha = 0.9;
	this.sccloud.scale.setTo(0.5,0.5);
			    	//this.sccloud.anchor.set(0.5);
	this.sccloud.animations.add('fly', Phaser.Animation.generateFrameNames('sc', 1, 5, '', 1), 20, true);
	this.sccloud.animations.play('fly');
	//creates score text
	this.display = game.add.text(950, 385, 'Score: ' + 0 + 's', {font: 'Verdana', fontSize: '20px', fill: '#000000'});
	this.score = 0;

	console.log("Entering next state");

	game.time.events.add(Phaser.Timer.SECOND * 0.6, restartP1, this);
}

function replay2() {
	this.display.destroy();
	this.sccloud.destroy();
	this.prompt.destroy();
	this.tornado.destroy();
	this.credits.destroy();
	this.tcloudFront.destroy(true);
	this.tcloudBack.destroy(true);
	if (this.crowsBack != null) {
		this.crowsBack.destroy(true);
	}
	if (this.crows != null) {
		this.crows.destroy(true);
	}
	if (this.streamlines != null) {
		this.streamlines.destroy(true);
	}
	//creates new scorecloud with refreshed values
	this.sccloud = game.add.sprite(845, 355, 'misc', 'sc1');
	this.sccloud.alpha = 0.9;
	this.sccloud.scale.setTo(0.5,0.5);
			    	//this.sccloud.anchor.set(0.5);
	this.sccloud.animations.add('fly', Phaser.Animation.generateFrameNames('sc', 1, 5, '', 1), 20, true);
	this.sccloud.animations.play('fly');
	//creates score text
	this.display = game.add.text(950, 385, 'Score: ' + 0 + 's', {font: 'Verdana', fontSize: '20px', fill: '#000000'});
	this.score = 0;

	console.log("Entering next state");

	game.time.events.add(Phaser.Timer.SECOND * 0.6, restartP12, this);
}

function replay3() {
	this.display.destroy();
	this.sccloud.destroy();
	this.prompt.destroy();
	this.tornado.destroy();
	this.credits.destroy();
	this.tcloudFront.destroy(true);
	this.tcloudBack.destroy(true);
	if (this.crowsBack != null) {
		this.crowsBack.destroy(true);
	}
	if (this.crows != null) {
		this.crows.destroy(true);
	}
	if (this.streamlines != null) {
		this.streamlines.destroy(true);
	}
	//creates new scorecloud with refreshed values
	this.sccloud = game.add.sprite(845, 355, 'misc', 'sc1');
	this.sccloud.alpha = 0.9;
	this.sccloud.scale.setTo(0.5,0.5);
			    	//this.sccloud.anchor.set(0.5);
	this.sccloud.animations.add('fly', Phaser.Animation.generateFrameNames('sc', 1, 5, '', 1), 20, true);
	this.sccloud.animations.play('fly');
	//creates score text
	this.display = game.add.text(950, 385, 'Score: ' + 0 + 's', {font: 'Verdana', fontSize: '20px', fill: '#000000'});
	this.score = 0;

	console.log("Entering next state");

	game.time.events.add(Phaser.Timer.SECOND * 0.6, restartP13, this);
}


//spawns in the prompt to replay the game (can be optimized using time delay inherent in tweens)
function tweenprompt() {
	game.add.tween(this.prompt).to({x: 300, y: 250}, 1500, Phaser.Easing.Quadratic.InOut, true);
}

// *Restart the game and fix tcloud
function restartP1(){
		game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, 0, this.tut, this.tut2, false);

	}
function restartP12(){
		game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, 0, false, false, false);
}

function restartP13(){
		game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, 0, false, false, true);
}
