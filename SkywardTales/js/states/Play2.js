/*
120 - Final Project 
*/

"use strict";

var play2tutorial = false;

var Play2 = function(game){};
Play2.prototype = {
	init: function(player, tornado, parallax, foreground, cloud, display, sccloud, score, tcloudFront, tcloudBack, score2, tut, tut2, tutorial) {
		this.player = player;
		this.tornado = tornado;
		this.parallax = parallax;
		this.foreground = foreground;
		this.cloud = cloud;
		this.display = display;
		this.sccloud = sccloud;
		this.score = score;
		this.score2 = score2;
		this.play2tutorial = tut;
		this.tut2 = tut2;
		this.tpState = ['flex', 'chill', 'flail', 'magic', 'spin'];
		this.p2;
		this.player2; 
		this.disable = false;		//boolean that fully disables all input when starting to transition back to Phase One
		this.internalTime = 0;		//time spent in this phase, moves back to phase one after certain duration
		this.length = 12;			//the time that will be elapsed in this phase; change this value for debugging as neccessary
		this.CLOUD_SPEED = 200;		// *The adjustable Speed of Cloud
		this.teleporting2 = false;
		this.weight = true;
		this.destinationX;
		this.destinationY;
		this.return = false;		//boolean that corrects spammable teleport behavior with upwards movement

		this.debris;
		this.debug = false;

		this.crowSpawn;
		this.streamlines;
		this.crows;
		this.crowsBack;
		this.splat;
		this.emitter;
		
		this.foop = false;

		this.tcloudFront = tcloudFront;
		this.tcloudBack = tcloudBack;
		this.rgb = 0xcd;
		this.cleanup = false;
		this.next3 = false;          
		this.invin = true;
		
		this.exe = true;
		this.ready = false;
		
		//this.boo = game.add.sprite(0, 0 , 'bar');
		this.tutorial = tutorial;
	},
	create: function() {
		console.log('Entered [Play2]');
		//keeps the parallax running in this state
		game.time.events.loop(Phaser.Timer.SECOND * 0.25, spawnParallax, this, 1);
		game.time.events.loop(Phaser.Timer.SECOND * 2, spawnParallax, this, 0);
		
		//Crow section----------------------------------------------------------
		this.streamlines = game.add.group();
		this.streamlines.enableBody = true;
		this.crowsBack = game.add.group();
		this.crowsBack.enableBody = true;
		this.crows = game.add.group();
		this.crows.enableBody = true;

		this.crowSpawn = game.time.create(false);
		if(!this.tutorial)
			this.crowSpawn.loop(Phaser.Timer.SECOND * 2.5, spawnCrows, this);
	    else
			this.crowSpawn.loop(Phaser.Timer.SECOND * 4, spawnCrows, this);
		//----------------------------------------------------------------------
		
		
		/*
		boo.events.onInputOver.add(function(){
		   this.game.canvas.style.cursor = "move"; }, this);
		   
		boo.events.onInputOut.add(function(){
		   this.game.canvas.style.cursor = "default"; }, this);
		*/
		
		
		
		//keeps the timer going in this state as well
		game.time.events.loop(Phaser.Timer.SECOND, timeScore2, this);
		game.time.events.loop(Phaser.Timer.SECOND * 0.1 , timeScoreOof2, this);
		
		if (!this.play2tutorial) {
			//extends period in this phase
			//this.length += 5;
			//creates tutorial assets if tutorial has never been seen
			if(!this.tutorial) {
				this.instructions = game.add.sprite(350, 800 + 330, 'misc', 'inst2');
				this.wsc = game.add.sprite(430, 800 + 450, 'misc', 'wsc1');
				this.wsc.animations.add('wasdcloud', Phaser.Animation.generateFrameNames('wsc', 1, 2, '', 1), 20, true);
				this.wsc.animations.play('wasdcloud');
				this.wsc.anchor.set(0.5);
				this.wasd = game.add.sprite(430, 800 + 450, 'misc', 'wasd1');
				this.wasd.animations.add('presswasd', Phaser.Animation.generateFrameNames('wasd', 1, 4, '', 1), 3, true);
				this.wasd.animations.play('presswasd');
				this.wasd.anchor.set(0.5);
				this.msc = game.add.sprite(650, 800 + 480, 'misc', 'msc1');
				this.msc.animations.add('mousecloud', Phaser.Animation.generateFrameNames('msc', 1, 2, '', 1), 20, true);
				this.msc.animations.play('mousecloud');
				this.msc.anchor.set(0.5);
				this.mouse = game.add.sprite(650, 800 + 480, 'misc', 'mouse1');
				this.mouse.animations.add('click', Phaser.Animation.generateFrameNames('mouse', 1, 2, '', 1), 3, true);
				this.mouse.animations.play('click');
				this.mouse.anchor.set(0.5);
				//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
				//this.credits = game.add.sprite(game.width + 700, game.height + 200, 'misc', 'name');
				//tweens title assets in
				game.add.tween(this.instructions).to ( { x: 350, y: 330 }, 3000, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.wsc).to( { x: 430, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.wasd).to ( { x: 430, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.msc).to( { x: 650, y: 280 }, 3000, Phaser.Easing.Quadratic.InOut, true);
				game.add.tween(this.mouse).to ( { x: 650, y: 280 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			} 
		}
		else {
			this.crowSpawn.start();
			this.invin = false;
		}
		
		if(this.tutorial) {
			this.instructions = game.add.sprite(350, 800 + 330, 'misc', 'inst2');
			this.wsc = game.add.sprite(430, 800 + 450, 'misc', 'wsc1');
			this.wsc.animations.add('wasdcloud', Phaser.Animation.generateFrameNames('wsc', 1, 2, '', 1), 20, true);
			this.wsc.animations.play('wasdcloud');
			this.wsc.anchor.set(0.5);
			this.wasd = game.add.sprite(430, 800 + 450, 'misc', 'wasd1');
			this.wasd.animations.add('presswasd', Phaser.Animation.generateFrameNames('wasd', 1, 4, '', 1), 3, true);
			this.wasd.animations.play('presswasd');
			this.wasd.anchor.set(0.5);
			this.msc = game.add.sprite(650, 800 + 480, 'misc', 'msc1');
			this.msc.animations.add('mousecloud', Phaser.Animation.generateFrameNames('msc', 1, 2, '', 1), 20, true);
			this.msc.animations.play('mousecloud');
			this.msc.anchor.set(0.5);
			this.mouse = game.add.sprite(650, 800 + 480, 'misc', 'mouse1');
			this.mouse.animations.add('click', Phaser.Animation.generateFrameNames('mouse', 1, 2, '', 1), 3, true);
			this.mouse.animations.play('click');
			this.mouse.anchor.set(0.5);
				//NOTE: [credits] are commented out because currently it has only my name on it, and that doesn't feel right; uncomment when fixed
				//this.credits = game.add.sprite(game.width + 700, game.height + 200, 'misc', 'name');
				//tweens title assets in
			
			game.world.bringToTop(this.instructions);
			game.world.bringToTop(this.wsc);
			game.world.bringToTop(this.wasd);
			game.world.bringToTop(this.msc);
			game.world.bringToTop(this.mouse);
			game.add.tween(this.instructions).to ( { x: 350, y: 330 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.wsc).to( { x: 430, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.wasd).to ( { x: 430, y: 250 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: 650, y: 280 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to ( { x: 650, y: 280 }, 3000, Phaser.Easing.Quadratic.InOut, true);
			
			
			
		}
		
		
		
		
		
		
		
		
		
	    this.player.alpha = 0;
	    this.player.x = -100;
	    this.player.y = -100;
	    this.player2 = game.add.group();
	    this.player.body.immovable = true;

    	// *Create the player2 for second phase
    	this.p2 = game.add.sprite(this.sccloud.x + 170,this.sccloud.y - 10,'pc','ride1');
		this.p2.animations.add('ride', Phaser.Animation.generateFrameNames('ride', 1, 4, '', 1), 15, true);
	    this.p2.animations.play('ride');
		
    	this.p2.anchor.set(0.5);
    	this.player2.addChild(this.p2);

    	// *Create the player2
    	this.player2.addChild(this.sccloud);

    	// *Restart the timer
    	this.shiftPhase = 0;
		
		this.debris = game.add.group();
		
    	// *Use the Physics for separate sprite before figured out the group solution
    	game.physics.arcade.enable(this.p2);
    	game.physics.arcade.enable(this.sccloud);
    	game.physics.arcade.enable(this.display);

    	//this.player2.setAllChildren(body.collideWorldBounds, true);

    	//creates teleport smoke asset
		this.smoke = game.add.sprite(-50, -50, 'pc', 'puff1');
		this.smoke.animations.add('puff', Phaser.Animation.generateFrameNames('puff', 1, 3, '', 1), 15, false);
		this.smoke.alpha = 0;
		this.smoke.anchor.set(0.5);

		// *Set physics for sccloud for catching
		this.sccloud.body.setSize(400, 110, 155, 38);
    	this.sccloud.body.immovable = true;
	

		this.p2.body.immovable = true;

		game.add.tween(this.tornado).to({x: game.width/4}, 2000, Phaser.Easing.Quadratic.InOut, true);

		//emitter stuff
		this.emitter = game.add.emitter(0, 0, 50);
		this.emitter.makeParticles('crow', ['feather1', 'feather2'], 50);
		this.emitter.width = 50;
		this.emitter.height = 50;
		this.emitter.minParticleSpeed.setTo(0, -100);
		this.emitter.maxParticleSpeed.setTo(300, 100);
		this.emitter.gravity.x = -1000;
		this.emitter.setScale(0.5, 0.75, 0.5, 0.75);

	    game.world.sendToBack(this.tornado);
		game.world.sendToBack(this.crowsBack);
	    game.world.sendToBack(this.tcloudBack);
		game.world.sendToBack(this.parallax);
		game.world.bringToTop(this.debris);
	    game.world.bringToTop(this.streamlines);
	    game.world.bringToTop(this.p2);
	    game.world.bringToTop(this.player);
	    game.world.bringToTop(this.sccloud);
	    game.world.bringToTop(this.display);
	    game.world.bringToTop(this.foreground);
	    game.world.bringToTop(this.tcloudFront);
		
		if(!this.play2tutorial || this.tutorial)
			game.time.events.add(Phaser.Timer.SECOND * 3, function(){this.ready = true;} , this);
		else
			this.ready = true;
	},
	update: function() {
		
		
		if(this.tutorial)
			this.crowSpawn.start();
		   
		if (game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && !this.play2tutorial && this.ready && !this.tutorial) {
			this.play2tutorial = true;
			var p2iclean = game.add.tween(this.instructions).to( { x: -800, y: 330 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.wsc).to( { x: -800, y: 250 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.wasd).to( { x: -800, y: 250 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: -800, y: 280 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to( { x: -800, y: 280 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			p2iclean.onComplete.add(p2icleanup, this);
			game.time.events.add(Phaser.Timer.SECOND * 3, invincibility, this);
			this.crowSpawn.start();
		}
		
		if(this.tutorial && this.ready && this.game.input.keyboard.isDown(Phaser.Keyboard.X)) {
			var p2iclean = game.add.tween(this.instructions).to( { x: -800, y: 330 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.wsc).to( { x: -800, y: 250 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.wasd).to( { x: -800, y: 250 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.msc).to( { x: -800, y: 280 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			game.add.tween(this.mouse).to( { x: -800, y: 280 }, 1500, Phaser.Easing.Quadratic.InOut, true);
			p2iclean.onComplete.add(p2icleanup, this);
			this.foop = true;
			this.ready = false;
		}
		
		
		// *Second Phase mechanics--------------------
		this.tcloudFront.subAll('tilePosition.x', 3);
		this.tcloudBack.subAll('tilePosition.x', 2);

		//If the debris and player overlap
		game.physics.arcade.collide(this.p2, this.debris, hitDebris, null, this);
		game.physics.arcade.collide(this.player, this.debris, hitDebrisTP, null, this);
		
		if(this.teleporting2 == false) 
			speed = 200;
		
		
		var speed = 450;
		
		if (this.input.keyboard.isDown(Phaser.Keyboard.W) && !this.disable && this.p2.y > 35 && !this.input.keyboard.isDown(Phaser.Keyboard.S)) {
		    //this.player2.setAllChildren(body.velocity.y, -100);
		    if(!this.weight) {
				this.p2.body.velocity.y = -speed;
				this.sccloud.body.velocity.y = -speed;
		        this.display.body.velocity.y = -speed;
			} else {
		       this.p2.body.velocity.y = -270;
		       this.sccloud.body.velocity.y = -270;
		       this.display.body.velocity.y = -270;
			}
		}
		else if (this.input.keyboard.isDown(Phaser.Keyboard.S) && !this.disable && this.p2.y < game.height - 100 && !this.input.keyboard.isDown(Phaser.Keyboard.W)) {
		    //this.player2.setAllChildren(body.velocity.y, 100);
		  if(!this.weight) {
				this.p2.body.velocity.y = speed;
				this.sccloud.body.velocity.y = speed;
		        this.display.body.velocity.y = speed;
			} else {
		       this.p2.body.velocity.y = 270;
		       this.sccloud.body.velocity.y = 270;
		       this.display.body.velocity.y = 270;
			}
		}
		else{
			this.p2.body.velocity.y = 0;
			this.sccloud.body.velocity.y = 0;
			this.display.body.velocity.y = 0;
		}
		
	    if (this.input.keyboard.isDown(Phaser.Keyboard.A) && !this.disable && this.p2.x > 50) {
		    //this.player2.setAllChildren(body.velocity.x, -100);
		   	if(!this.weight) {
				this.p2.body.velocity.x = -speed;
				this.sccloud.body.velocity.x = -speed;
		        this.display.body.velocity.x = -speed;
			} else {
		       this.p2.body.velocity.x = -270;
		       this.sccloud.body.velocity.x = -270;
		       this.display.body.velocity.x = -270;
			}
		}
		else if (this.input.keyboard.isDown(Phaser.Keyboard.D) && !this.disable && this.p2.x < game.width - 50 && !this.input.keyboard.isDown(Phaser.Keyboard.A)) {
		    //this.player2.setAllChildren(body.velocity.x, 100);
			if(!this.weight) {
				this.p2.body.velocity.x = speed;
				this.sccloud.body.velocity.x = speed;
		        this.display.body.velocity.x = speed;
			} else {
		       this.p2.body.velocity.x = 270;
		       this.sccloud.body.velocity.x = 270;
		       this.display.body.velocity.x = 270;
			}
		}
		else{
			this.p2.body.velocity.x = 0;
			this.sccloud.body.velocity.x = 0;
			this.display.body.velocity.x = 0;
		}

		// *Panel Spawning
		/* NOT IMPLEMENTED */

		//scales tint in and out of dark clouds
		this.tcloudFront.setAll('tint', (this.rgb * 0x10000) + (this.rgb * 0x100) + this.rgb);
		this.tcloudBack.setAll('tint', (this.rgb * 0x10000) + (this.rgb * 0x100) + this.rgb);

		// *Player teleport 
		if(game.input.activePointer.leftButton.isDown && !this.teleporting2 && !this.disable && this.exe) {
			this.return = false;
			this.teleporting2 = true;
			this.destinationX = game.input.mousePointer.x;
			this.destinationY = game.input.mousePointer.y;
			this.smoke.x = this.p2.x;
			this.smoke.y = this.p2.y;
			this.smoke.angle = Math.random() * 360;
			this.smoke.alpha = 1;
			this.smoke.bringToTop();
			var music = game.sound.play('poof');
			this.smoke.animations.play('puff');

			console.log('Teleport-2');
			this.p2.alpha = 0;
			this.weight = false;
			game.time.events.add(Phaser.Timer.SECOND * 0.1, tpDust2, this);
			game.time.events.add(Phaser.Timer.SECOND * 0.1, teleport2, this);
		}

		//crow shift phase checker
		var fcrow = this.crowsBack.getFurthestFrom(this.tornado);
		if (fcrow != null && fcrow.position.x > game.width + 100) {
			var buffer = this.crowsBack.removeChild(fcrow);
			var type = 0;
			if (fcrow.animations.name == 'sweep') {
				type = 1;
			}
			var newCrow = new CrowFront(game, 'crow', buffer.x, (300+((buffer.y -300)* 2)), -400, this.streamlines, type);
			game.add.existing(newCrow);
			this.debris.add(newCrow);
			buffer.destroy();
			console.log("Switched directions");
		}

		//being hit checker
		if (this.player.animations.name == 'dazed') {
			this.player.body.angularVelocity = -50;
		}

		// *Cloud catching
		game.physics.arcade.collide(this.player, this.sccloud, this.cloudCatching, null, this);

		// Cleanup if statement
		if ((this.internalTime >= this.length) || (this.tutorial && this.foop)) {
			this.crowSpawn.destroy();
			//this.exe = false;
			game.time.events.add(Phaser.Timer.SECOND * 5, cleanUpdate, this);
		}

		// *Getting back to First Phase when timer calls
		if((this.internalTime >= this.length && !this.disable && !this.teleporting2 && this.cleanup == true && !this.tutorial) || (this.tutorial && this.cleanup)){
			game.time.events.repeat(0.05, 50, tweenTintBack, this);
			var delay = this.tornado.animations.play('diminish', 10, false, true);
			this.teleporting2 = true;
			this.disable = true;
	    	// *Stop movements
	    	this.p2.body.velocity.x = 0;
			this.sccloud.body.velocity.x = 0;
			this.display.body.velocity.x = 0;
	    	this.p2.body.velocity.y = 0;
			this.sccloud.body.velocity.y = 0;
			this.display.body.velocity.y = 0;

			delay.onComplete.add(preparation2, this);
	  //   	// *Move player back to desired position
	  //   	game.add.tween(this.p2).to( { x: 50, y: 275}, 2000, Phaser.Easing.Quadratic.InOut, true);
	  //   	//game.add.tween(this.sccloud).to( { x: 40, y: 350}, 2000, Phaser.Easing.Quadratic.InOut, true);
	  //   	game.add.tween(this.sccloud).to( { x: -110, y: 295}, 2000, Phaser.Easing.Quadratic.InOut, true);
			// game.add.tween(this.display).to( { x: -5, y: 325}, 2000, Phaser.Easing.Quadratic.InOut, true);
			// this.player.body.immovable = false;

			// var buffer = this.tcloudFront.getBottom();
			// game.add.tween(buffer).to({y: -400}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.In, true);
			// buffer = this.tcloudFront.getTop();
			// game.add.tween(buffer).to({y: 600}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.In, true);

			// game.time.events.add(2000, jumpReturn, this);
		}
		if (this.smoke.alpha > 0) {
			var dust = new Puff(game, 'pc', this.smoke.x, this.smoke.y);
			game.add.existing(dust);
			this.smoke.alpha -= 0.05;
		}
		
		if (this.input.keyboard.justPressed(Phaser.Keyboard.O)) {
		    this.debug = !this.debug;
		}
		
		if (this.player.y > 700) {

            if(this.invin) {
				this.destinationX = this.sccloud.body.x;
			    this.destinationY = this.sccloud.body.y - 100;
				game.time.events.add(Phaser.Timer.SECOND * 0, tpDust2, this);
		    	game.time.events.add(Phaser.Timer.SECOND * 0, teleport2, this);
			}
			else {
			   this.smoke.destroy();
			   this.p2.body.velocity.x = 0;
			   this.sccloud.body.velocity.x = 0;
			   this.display.body.velocity.x = 0;
	    	   this.p2.body.velocity.y = 0;
			   this.sccloud.body.velocity.y = 0;
			   this.display.body.velocity.y = 0;
			
			   if (this.splat != null) 
				  this.splat.destroy();
			
			   game.state.start('End', false, false, this.player, this.tornado, this.parallax, this.foreground, this.cloud, this.display, this.sccloud, this.debris, this.tcloudFront, this.tcloudBack, this.crowsBack, this.crows, this.streamlines, 2, this.play2tutorial, this.tut2, this.tutorial);
			}
	    }

	    //emitter stuff
	    // this.emitter.forEachAlive(function(p) {
	    // 	p.alpha -= 0.01;
	   	// });
	},
	cloudCatching: function(player,sccloud) {
		if (this.return) {
			this.return = false;
			player.body.gravity.y = 0;
			player.body.velocity.y = 0;
			player.x = -100;
			player.y = -100;
			player.alpha = 0;
			this.p2.alpha = 1;
			this.weight = true;
			game.time.events.add(Phaser.Timer.SECOND * 0.5, this.tpCD, this);
		}
	},
	tpCD: function() {
		this.teleporting2 = false;
	},
	render: function() {
		if (this.debug) {
			game.debug.text('Teleporting2: [' + this.teleporting2 + ']', 32, 80, "#ffffff");
			game.debug.text('play2tutorial boolean: [' + this.crowsBack.length + ']', 32, 96, "#ffffff");
			game.debug.physicsGroup(this.crows);
		}
	}
}

function hitDebris(p2, debris) {
	if(p2.alpha == 1){
		console.log('debris hit');
		if(!this.tutorial) {
		   this.disable = true;
		   this.sccloud.body.checkCollision.none = true;
		} 
		this.debris.setAll('body.checkCollision.none', true); 
		this.p2.alpha = 0;
		this.player.x = this.p2.x;
		this.player.y = this.p2.y;
		this.player.body.velocity.y = -200;
		this.player.body.gravity.y = 1800;
		this.player.body.velocity.x = -200;
		this.player.animations.play('dazed');
		this.player.alpha = 1;

		//splats and feather burst
		this.emitter.x = this.player.x;
		this.emitter.y = this.player.y;
		this.emitter.start(true, 1000, null, 20);
		this.splat = game.add.sprite(this.player.x - 200, this.player.y - 30, 'crow', 'crowburst1');
		this.splat.scale.setTo(1.5, 1.5);
		this.splat.animations.add('splat', Phaser.Animation.generateFrameNames('crowburst', 1, 4, '', 1), 20, false);
		this.splat.animations.play('splat');
		game.add.tween(this.splat).to( {alpha: 0}, 750, Phaser.Easing.Linear.None, true);
		debris.destroy();
	}
}
function hitDebrisTP(player, debris) {
	if(this.p2.alpha == 0 && this.return == true)
	console.log('debris hit during teleporting');
    if(!this.tutorial) {
	   this.disable = true;
       this.sccloud.body.checkCollision.none = true;
	}
	this.debris.setAll('body.checkCollision.none', true);
	this.player.body.velocity.y = -100;
		this.player.body.velocity.x = -200;
	this.player.animations.play('dazed');

	//splats and feather burst
	this.emitter.x = this.player.x;
	this.emitter.y = this.player.y;
	this.emitter.start(true, 1000, null, 20);
	this.splat = game.add.sprite(this.player.x - 200, this.player.y - 30, 'crow', 'crowburst1');
	this.splat.scale.setTo(1.5, 1.5);
	this.splat.animations.add('splat', Phaser.Animation.generateFrameNames('crowburst', 1, 4, '', 1), 20, false);
	this.splat.animations.play('splat');
	game.add.tween(this.splat).to( {alpha: 0}, 750, Phaser.Easing.Linear.None, true);
	debris.destroy();
}

function preparation2() {
	// *Move player back to desired position
	game.add.tween(this.p2).to( { x: 50, y: 275}, 2000, Phaser.Easing.Quadratic.InOut, true);
	//game.add.tween(this.sccloud).to( { x: 40, y: 350}, 2000, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(this.sccloud).to( { x: -110, y: 295}, 2000, Phaser.Easing.Quadratic.InOut, true);
	game.add.tween(this.display).to( { x: -20, y: 325}, 2000, Phaser.Easing.Quadratic.InOut, true);
	this.player.body.immovable = false;

	var buffer = this.tcloudFront.getBottom();
	game.add.tween(buffer).to({y: -400}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.In, true);
	buffer = this.tcloudBack.getBottom();
	game.add.tween(buffer).to({y: -400}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.In, true);
	buffer = this.tcloudFront.getTop();
	game.add.tween(buffer).to({y: 600}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.In, true);
	buffer = this.tcloudBack.getTop();
	game.add.tween(buffer).to({y: 600}, Phaser.Timer.SECOND * 1.5, Phaser.Easing.Quadratic.In, true);

	game.time.events.add(2000, jumpReturn, this);
}

//micro function of timeScore for Play2
function timeScore2() {
	if(this.play2tutorial) {
	   this.score += 1;
	   this.internalTime += 1;
	}
	//game.add.tween(this.tornado).to( {x: (Math.random() * 40) + game.width/4 - 20}, 5000, Phaser.Easing.Quadratic.InOut, true);
}

function timeScoreOof2() {
	if(this.play2tutorial) {
	   this.score2 += 1;
	}
	if(this.score2 <= 1000)
       this.display.text = 'Distance: ' + this.score2 + 'm';
    else
	   this.display.text = 'Distance: ' + Math.floor(this.score2/1000) + '.' + Math.floor(((this.score2%1000)/100)%10) +  Math.floor(((this.score2%1000)/10)%10) + 'km';
   if(this.tutorial)
	   this.display.text = '    TUTORIAL';
}

function invincibility() {
   this.invin = false;	
}

//function that returns the player back to the 'Play' state after cleanup
function jumpReturn() {
	// *Make the player jump from the cloud
    this.player.x = this.p2.x;
    this.player.y = this.p2.y;
    this.tcloudFront.destroy(true);
    this.tcloudBack.destroy(true);
    this.p2.destroy();
    this.player.alpha = 1;
    this.player.body.gravity.y = 1800;
    this.player.body.velocity.y = -500;
	console.log('Jump return');
    this.player.animations.play('jump');
    //return to Phase 1
	this.debris.destroy();
	//****************************************************************************
	this.next3 = true;
	this.smoke.destroy();
	if(!this.tutorial)
	   game.state.start('Play', false, false, this.player, this.parallax, this.foreground, this.score, this.display, this.sccloud, this.next3, this.score2, this.play2tutorial, this.tut2, this.tutorial);
   else
      game.state.start('End', false, false, this.player, this.tornado, this.parallax, this.foreground, this.cloud, this.display, this.sccloud, this.debris, this.tcloudFront, this.tcloudBack, this.crowsBack, this.crows, this.streamlines, 2, this.play2tutorial, this.tut2, this.tutorial);
}


//creates a smoke cloud and puts it at the player location
function tpDust2() {
	//var music = game.sound.play('poof');
	if(this.invin && this.player.y > 700)
	   this.smoke.x = this.destinationX + 100;
    else
	   this.smoke.x = this.destinationX;
	this.smoke.y = this.destinationY
	this.smoke.angle = Math.random() * 360;
	this.smoke.alpha = 1;
	this.smoke.animations.play('puff');
}

//teleports the player back onto the stage by making them visible again
function teleport2() {
	if(this.invin && this.player.y > 700)
	   this.player.x = this.destinationX + 100;
    else
	   this.player.x = this.destinationX;
    this.player.y = this.destinationY;
	this.player.body.velocity.y = -100;
	this.player.body.gravity.y = 700;
	this.player.body.velocity.x = 0;
	this.player.body.angularVelocity = 0;
	this.player.body.angle = 0;
	this.player.animations.play(Phaser.ArrayUtils.getRandomItem(this.tpState));
	this.player.alpha = 1;
	this.return = true;
}

function spawnCrows() {
	var formation = game.rnd.integerInRange(0, 6);
	//if(this.tutorial)
	//	formation = 6;

	if (formation == 0) {
		game.time.events.add(0, spawnDart, this, 300);
		game.time.events.add(500, spawnDart, this, 200);
		game.time.events.add(500, spawnDart, this, 400);
	}
	// else if (formation == 1) {
	// 	game.time.events.add(0, spawnSweep, this, 250);
	// 	game.time.events.add(0, spawnSweep, this, 350);
	// }
	else if (formation == 1/*2*/) {
		game.time.events.add(0, spawnSweep, this, 300);
		game.time.events.add(500, spawnSweep, this, 200);
		game.time.events.add(1000, spawnSweep, this, 400);
	}
	else if (formation == 2/*3*/) {
		//game.time.events.add(0, spawnDart, this, 300);
		game.time.events.add(300, spawnDart, this, 250);
		game.time.events.add(300, spawnDart, this, 350);
		game.time.events.add(600, spawnDart, this, 200);
		game.time.events.add(600, spawnDart, this, 400);
	}
	else if (formation == 3/*4*/) {
		var rheight = game.rnd.integerInRange(200, 400);
		game.time.events.add(0, spawnDart, this, rheight);
		game.time.events.add(300, spawnDart, this, rheight);
		game.time.events.add(600, spawnDart, this, rheight);
	}
	else if (formation == 4/*5*/) {
		var rheight = game.rnd.integerInRange(200, 400);
		game.time.events.add(0, spawnSweep, this, rheight);
		game.time.events.add(300, spawnSweep, this, rheight);
		game.time.events.add(600, spawnSweep, this, rheight);
	}
	// else if (formation == 6) {
	// 	game.time.events.add(0, spawnDart, this, 200);
	// 	game.time.events.add(300, spawnDart, this, 250);
	// 	game.time.events.add(600, spawnDart, this, 300);
	// 	game.time.events.add(900, spawnDart, this, 350);
	// 	game.time.events.add(1200, spawnDart, this, 400);
	// }
	// else if (formation == 7) {
	// 	game.time.events.add(0, spawnDart, this, 400);
	// 	game.time.events.add(300, spawnDart, this, 350);
	// 	game.time.events.add(600, spawnDart, this, 300);
	// 	game.time.events.add(900, spawnDart, this, 250);
	// 	game.time.events.add(1200, spawnDart, this, 200);
	// }
	else if (formation == 5/*8*/) {
		game.time.events.add(0, spawnSweep, this, 200);
		game.time.events.add(0, spawnSweep, this, 400);
	}
	else if (formation == 6/*9*/) {
		game.time.events.add(0, spawnDart, this, 200);
		game.time.events.add(0, spawnDart, this, 250);
		//game.time.events.add(0, spawnDart, this, 300);
		game.time.events.add(0, spawnDart, this, 350);
		game.time.events.add(0, spawnDart, this, 400);
	}
	// else if (formation == 10) {
	// 	game.time.events.add(0, spawnSweep, this, 200);
	// 	game.time.events.add(0, spawnSweep, this, 400);
	// 	game.time.events.add(800, spawnSweep, this, 250);
	// 	game.time.events.add(800, spawnSweep, this, 350);
	// }
}

function spawnDart(y) {
	var newCrow = new Crow(game, 'crow', this.tornado.x - 50, y, -400, 0);
	game.add.existing(newCrow);
	this.crowsBack.add(newCrow);
}

function spawnSweep(y) {
	var newCrow = new Crow(game, 'crow', this.tornado.x - 50, y, -400, 1);
	game.add.existing(newCrow);
	this.crowsBack.add(newCrow);
}

function tweenTintBack() {
	if(!this.tutorial)
    this.rgb++;
}

function cleanUpdate() {
	this.cleanup = true;
}

function p2icleanup() {
	this.instructions.destroy();
	this.wsc.destroy();
	this.wasd.destroy();
	this.msc.destroy();
	this.mouse.destroy();
}